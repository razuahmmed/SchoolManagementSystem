/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam;

import com.entity.StuffInformationTable;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Maksud Rahaman
 */
@Controller
public class TeacehrContollerClass {
    public ModelLayer findMyBean() {
        GenericXmlApplicationContext context = new GenericXmlApplicationContext();
        context.load("classpath:appContext.xml");
        context.refresh();
        ModelLayer modelBean = context.getBean("modelClass", ModelLayer.class);
        return modelBean;
    }
//    @RequestMapping("/addNewTeacher")
//    public String addNewTeacher(@ModelAttribute("teacher")StuffInformationTable stuff){
//        ModelLayer modelBean = findMyBean();
//        boolean b = modelBean.insertToAddNewTeacher(stuff);
//        return "teacherAddForm";
//    }
}

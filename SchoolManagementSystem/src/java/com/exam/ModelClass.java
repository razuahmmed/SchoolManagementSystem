/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam;

import com.entity.AdminLoginTable;
import com.entity.ClassEightAdmissionTable;
import com.entity.ClassEightAdmissionTestTable;
import com.entity.ClassEightAttendanceTable;
import com.entity.ClassEightPaymentTable;
import com.entity.ClassEightResultTable;
import com.entity.ClassFiveAdmissionTable;
import com.entity.ClassFiveAdmissionTestTable;
import com.entity.ClassFiveAttendanceTable;
import com.entity.ClassFivePaymentTable;
import com.entity.ClassFiveResultTable;
import com.entity.ClassFourAdmissionTable;
import com.entity.ClassFourAdmissionTestTable;
import com.entity.ClassFourAttendanceTable;
import com.entity.ClassFourPaymentTable;
import com.entity.ClassFourResultTable;
import com.entity.ClassNineAdmissionTable;
import com.entity.ClassNineAdmissionTestTable;
import com.entity.ClassNineAttendanceTable;
import com.entity.ClassNinePaymentTable;
import com.entity.ClassNineResultTable;
import com.entity.ClassOneAdmissionTable;
import com.entity.ClassOneAdmissionTestTable;
import com.entity.ClassOneAttendanceTable;
import com.entity.ClassOnePaymentTable;
import com.entity.ClassOneResultTable;
import com.entity.ClassSevenAdmissionTable;
import com.entity.ClassSevenAdmissionTestTable;
import com.entity.ClassSevenAttendanceTable;
import com.entity.ClassSevenPaymentTable;
import com.entity.ClassSevenResultTable;
import com.entity.ClassSixAdmissionTable;
import com.entity.ClassSixAdmissionTestTable;
import com.entity.ClassSixAttendanceTable;
import com.entity.ClassSixPaymentTable;
import com.entity.ClassSixResultTable;
import com.entity.ClassTenAttendanceTable;
import com.entity.ClassTenPaymentTable;
import com.entity.ClassTenResultTable;
import com.entity.ClassTenStudentInformationTable;
import com.entity.ClassThreeAdmissionTable;
import com.entity.ClassThreeAdmissionTestTable;
import com.entity.ClassThreeAttendanceTable;
import com.entity.ClassThreePaymentTable;
import com.entity.ClassThreeResultTable;
import com.entity.ClassTwoAdmissionTable;
import com.entity.ClassTwoAdmissionTestTable;
import com.entity.ClassTwoAttendanceTable;
import com.entity.ClassTwoPaymentTable;
import com.entity.ClassTwoResultTable;
import com.entity.StuffInformationTable;
import java.sql.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Maksud Rahaman
 */
public class ModelClass implements ModelLayer {

    @Override
    public void insertClassOneAdmissionTest(ClassOneAdmissionTestTable adTestOne) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adTestOne);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassTwoAdmissionTest(ClassTwoAdmissionTestTable adTestTwo) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adTestTwo);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassThreeAdmissionTest(ClassThreeAdmissionTestTable adTestThree) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adTestThree);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassFourAdmissionTest(ClassFourAdmissionTestTable adTestFour) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adTestFour);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassFiveAdmissionTest(ClassFiveAdmissionTestTable adTestFive) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adTestFive);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassSixAdmissionTest(ClassSixAdmissionTestTable adTestSix) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adTestSix);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassSevenAdmissionTest(ClassSevenAdmissionTestTable adTestSeven) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adTestSeven);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassEightAdmissionTest(ClassEightAdmissionTestTable adTestEight) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adTestEight);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassNineAdmissionTest(ClassNineAdmissionTestTable adTestNine) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adTestNine);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    //Start Query From all AdmissionTestTable to get admit card
    @Override
    public List<ClassOneAdmissionTestTable> findAdmitCardClassOne(String name, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassOneAdmissionTestTable where studentName=:a and email=:b");
        q.setParameter("a", name);
        q.setParameter("b", email);
        List<ClassOneAdmissionTestTable> list = q.list();
        s.getTransaction().commit();
        s.close();
        return list;

    }

    @Override
    public List<ClassTwoAdmissionTestTable> findAdmitCardClassTwo(String name, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassTwoAdmissionTestTable where studentName=:a and email=:b");
        q.setParameter("a", name);
        q.setParameter("b", email);
        List<ClassTwoAdmissionTestTable> list = q.list();
        s.getTransaction().commit();
        s.close();
        return list;

    }

    @Override
    public List<ClassThreeAdmissionTestTable> findAdmitCardClassThree(String name, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassThreeAdmissionTestTable where studentName=:a and email=:b");
        q.setParameter("a", name);
        q.setParameter("b", email);
        List<ClassThreeAdmissionTestTable> list = q.list();
        s.getTransaction().commit();
        s.close();
        return list;

    }

    @Override
    public List<ClassFourAdmissionTestTable> findAdmitCardClassFour(String name, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFourAdmissionTestTable where studentName=:a and email=:b");
        q.setParameter("a", name);
        q.setParameter("b", email);
        List<ClassFourAdmissionTestTable> list = q.list();
        s.getTransaction().commit();
        s.close();
        return list;

    }

    @Override
    public List<ClassFiveAdmissionTestTable> findAdmitCardClassFive(String name, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFiveAdmissionTestTable where studentName=:a and email=:b");
        q.setParameter("a", name);
        q.setParameter("b", email);
        List<ClassFiveAdmissionTestTable> list = q.list();
        s.getTransaction().commit();
        s.close();
        return list;

    }

    @Override
    public List<ClassSixAdmissionTestTable> findAdmitCardClassSix(String name, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSixAdmissionTestTable where studentName=:a and email=:b");
        q.setParameter("a", name);
        q.setParameter("b", email);
        List<ClassSixAdmissionTestTable> list = q.list();
        s.getTransaction().commit();
        s.close();
        return list;

    }

    @Override
    public List<ClassSevenAdmissionTestTable> findAdmitCardClassSeven(String name, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSevenAdmissionTestTable where studentName=:a and email=:b");
        q.setParameter("a", name);
        q.setParameter("b", email);
        List<ClassSevenAdmissionTestTable> list = q.list();
        s.getTransaction().commit();
        s.close();
        return list;

    }

    @Override
    public List<ClassEightAdmissionTestTable> findAdmitCardClassEight(String name, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassEightAdmissionTestTable where studentName=:a and email=:b");
        q.setParameter("a", name);
        q.setParameter("b", email);
        List<ClassEightAdmissionTestTable> list = q.list();
        s.getTransaction().commit();
        s.close();
        return list;

    }

    @Override
    public List<ClassNineAdmissionTestTable> findAdmitCardClassNine(String name, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassNineAdmissionTestTable where studentName=:a and email=:b");
        q.setParameter("a", name);
        q.setParameter("b", email);
        List<ClassNineAdmissionTestTable> list = q.list();
        s.getTransaction().commit();
        s.close();
        return list;

    }
    //End Query From all AdmissionTestTable to get admit card

    //Start Query From all AdmissionTestTables to confirm Admission
    @Override
    public List<ClassOneAdmissionTestTable> confirmAdmissionClassOne(int serialNumber, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassOneAdmissionTestTable where serialNumberOneadtest=:a and email=:b");
        q.setParameter("a", serialNumber);
        q.setParameter("b", email);
        List<ClassOneAdmissionTestTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassTwoAdmissionTestTable> confirmAdmissionClassTwo(int serialNumber, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassTwoAdmissionTestTable where serialNumberTwoadtest=:a and email=:b");
        q.setParameter("a", serialNumber);
        q.setParameter("b", email);
        List<ClassTwoAdmissionTestTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassThreeAdmissionTestTable> confirmAdmissionClassThree(int serialNumber, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassThreeAdmissionTestTable where serialNumberThreeadtest=:a and email=:b");
        q.setParameter("a", serialNumber);
        q.setParameter("b", email);
        List<ClassThreeAdmissionTestTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassFourAdmissionTestTable> confirmAdmissionClassFour(int serialNumber, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFourAdmissionTestTable where serialNumberFouradtest=:a and email=:b");
        q.setParameter("a", serialNumber);
        q.setParameter("b", email);
        List<ClassFourAdmissionTestTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassFiveAdmissionTestTable> confirmAdmissionClassFive(int serialNumber, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFiveAdmissionTestTable where serialNumberFiveadtest=:a and email=:b");
        q.setParameter("a", serialNumber);
        q.setParameter("b", email);
        List<ClassFiveAdmissionTestTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassSixAdmissionTestTable> confirmAdmissionClassSix(int serialNumber, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSixAdmissionTestTable where serialNumberSixadtest=:a and email=:b");
        q.setParameter("a", serialNumber);
        q.setParameter("b", email);
        List<ClassSixAdmissionTestTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassSevenAdmissionTestTable> confirmAdmissionClassSeven(int serialNumber, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSevenAdmissionTestTable where serialNumberSevenadtest=:a and email=:b");
        q.setParameter("a", serialNumber);
        q.setParameter("b", email);
        List<ClassSevenAdmissionTestTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassEightAdmissionTestTable> confirmAdmissionClassEight(int serialNumber, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassEightAdmissionTestTable where serialNumberEightadtest=:a and email=:b");
        q.setParameter("a", serialNumber);
        q.setParameter("b", email);
        List<ClassEightAdmissionTestTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassNineAdmissionTestTable> confirmAdmissionClassNine(int serialNumber, String email) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassNineAdmissionTestTable where serialNumberNineadtest=:a and email=:b");
        q.setParameter("a", serialNumber);
        q.setParameter("b", email);
        List<ClassNineAdmissionTestTable> list = q.list();
        return list;

    }
    //End of Query From all AdmissionTestTables to confirm Admission

    //Start inserting at AdmissionTables
    @Override
    public void insertClassOneAdmission(ClassOneAdmissionTable adOne) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adOne);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassTwoAdmission(ClassTwoAdmissionTable adTwo) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adTwo);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassThreeAdmission(ClassThreeAdmissionTable adThree) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adThree);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassFourAdmission(ClassFourAdmissionTable adFour) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adFour);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassFiveAdmission(ClassFiveAdmissionTable adFive) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adFive);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassSixAdmission(ClassSixAdmissionTable adSix) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adSix);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassSevenAdmission(ClassSevenAdmissionTable adSeven) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adSeven);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassEightAdmission(ClassEightAdmissionTable adEight) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adEight);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertClassNineAdmission(ClassNineAdmissionTable adNine) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adNine);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }
    //End of inserting at AdmissionTables

    //Start Query for ShowInformationForAdmissionRollNumber
    @Override
    public List<ClassOneAdmissionTable> findAdmissionInformationForRollNumberClassOne(int serialNumber) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassOneAdmissionTable where classOneAdmissionTestTable=:a");
        ClassOneAdmissionTestTable adTest = new ClassOneAdmissionTestTable();
        adTest.setSerialNumberOneadtest(serialNumber);
        q.setParameter("a", adTest);
        List<ClassOneAdmissionTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassTwoAdmissionTable> findAdmissionInformationForRollNumberClassTwo(int serialNumber) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassTwoAdmissionTable where classTwoAdmissionTestTable=:a");
        ClassTwoAdmissionTestTable adTest = new ClassTwoAdmissionTestTable();
        adTest.setSerialNumberTwoadtest(serialNumber);
        q.setParameter("a", adTest);
        List<ClassTwoAdmissionTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassThreeAdmissionTable> findAdmissionInformationForRollNumberClassThree(int serialNumber) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassThreeAdmissionTable where classThreeAdmissionTestTable=:a");
        ClassThreeAdmissionTestTable adTest = new ClassThreeAdmissionTestTable();
        adTest.setSerialNumberThreeadtest(serialNumber);
        q.setParameter("a", adTest);
        List<ClassThreeAdmissionTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassFourAdmissionTable> findAdmissionInformationForRollNumberClassFour(int serialNumber) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFourAdmissionTable where classFourAdmissionTestTable=:a");
        ClassFourAdmissionTestTable adTest = new ClassFourAdmissionTestTable();
        adTest.setSerialNumberFouradtest(serialNumber);
        q.setParameter("a", adTest);
        List<ClassFourAdmissionTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassFiveAdmissionTable> findAdmissionInformationForRollNumberClassFive(int serialNumber) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFiveAdmissionTable where classFiveAdmissionTestTable=:a");
        ClassFiveAdmissionTestTable adTest = new ClassFiveAdmissionTestTable();
        adTest.setSerialNumberFiveadtest(serialNumber);
        q.setParameter("a", adTest);
        List<ClassFiveAdmissionTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassSixAdmissionTable> findAdmissionInformationForRollNumberClassSix(int serialNumber) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSixAdmissionTable where classSixAdmissionTestTable=:a");
        ClassSixAdmissionTestTable adTest = new ClassSixAdmissionTestTable();
        adTest.setSerialNumberSixadtest(serialNumber);
        q.setParameter("a", adTest);
        List<ClassSixAdmissionTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassSevenAdmissionTable> findAdmissionInformationForRollNumberClassSeven(int serialNumber) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSevenAdmissionTable where classSevenAdmissionTestTable=:a");
        ClassSevenAdmissionTestTable adTest = new ClassSevenAdmissionTestTable();
        adTest.setSerialNumberSevenadtest(serialNumber);
        q.setParameter("a", adTest);
        List<ClassSevenAdmissionTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassEightAdmissionTable> findAdmissionInformationForRollNumberClassEight(int serialNumber) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassEightAdmissionTable where classEightAdmissionTestTable=:a");
        ClassEightAdmissionTestTable adTest = new ClassEightAdmissionTestTable();
        adTest.setSerialNumberEightadtest(serialNumber);
        q.setParameter("a", adTest);
        List<ClassEightAdmissionTable> list = q.list();
        return list;

    }

    @Override
    public List<ClassNineAdmissionTable> findAdmissionInformationForRollNumberClassNine(int serialNumber) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassNineAdmissionTable where classNineAdmissionTestTable=:a");
        ClassNineAdmissionTestTable adTest = new ClassNineAdmissionTestTable();
        adTest.setSerialNumberNineadtest(serialNumber);
        q.setParameter("a", adTest);
        List<ClassNineAdmissionTable> list = q.list();
        return list;

    }
    //End Query for ShowInformationForAdmissionRollNumber

   

    @Override
    public boolean adminLoginConfirm(String email, String password, String admincode) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from AdminLoginTable where email=:a and password=:b and admincode=:c");
        q.setParameter("a", email);
        q.setParameter("b", password);
        q.setParameter("c", admincode);
        List list = q.list();
        if (list.size() > 0) {
            s.close();
            return true;
        } else {
            s.close();
            return false;
        }
    }

    //Start querying from all AdmissionTables for payments
    @Override
    public List<ClassOneAdmissionTable> findAllInformationForFeesPaymentClassOne() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassOneAdmissionTable> list = s.createCriteria(ClassOneAdmissionTable.class).list();
        s.getTransaction().commit();
        return list;

    }

    @Override
    public List<ClassTwoAdmissionTable> findAllInformationForFeesPaymentClassTwo() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassTwoAdmissionTable> list = s.createCriteria(ClassTwoAdmissionTable.class).list();
        s.getTransaction().commit();
        return list;

    }

    @Override
    public List<ClassThreeAdmissionTable> findAllInformationForFeesPaymentClassThree() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassThreeAdmissionTable> list = s.createCriteria(ClassThreeAdmissionTable.class).list();
        s.getTransaction().commit();
        return list;

    }

    @Override
    public List<ClassFourAdmissionTable> findAllInformationForFeesPaymentClassFour() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassFourAdmissionTable> list = s.createCriteria(ClassFourAdmissionTable.class).list();
        s.getTransaction().commit();
        return list;

    }

    @Override
    public List<ClassFiveAdmissionTable> findAllInformationForFeesPaymentClassFive() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassFiveAdmissionTable> list = s.createCriteria(ClassFiveAdmissionTable.class).list();
        s.getTransaction().commit();
        return list;

    }

    @Override
    public List<ClassSixAdmissionTable> findAllInformationForFeesPaymentClassSix() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassSixAdmissionTable> list = s.createCriteria(ClassSixAdmissionTable.class).list();
        s.getTransaction().commit();
        return list;

    }

    @Override
    public List<ClassSevenAdmissionTable> findAllInformationForFeesPaymentClassSeven() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassSevenAdmissionTable> list = s.createCriteria(ClassSevenAdmissionTable.class).list();
        s.getTransaction().commit();
        return list;

    }

    @Override
    public List<ClassEightAdmissionTable> findAllInformationForFeesPaymentClassEight() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassEightAdmissionTable> list = s.createCriteria(ClassEightAdmissionTable.class).list();
        s.getTransaction().commit();
        return list;

    }

    @Override
    public List<ClassNineAdmissionTable> findAllInformationForFeesPaymentClassNine() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassNineAdmissionTable> list = s.createCriteria(ClassNineAdmissionTable.class).list();
        s.getTransaction().commit();
        return list;

    }

    @Override
    public List<ClassTenStudentInformationTable> findAllInformationForFeesPaymentClassTen() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassTenStudentInformationTable> list = s.createCriteria(ClassTenStudentInformationTable.class).list();
        s.getTransaction().commit();
        return list;

    }
    //End of querying from all AdmissionTables for payments

    //Start Inseting for creating Result
    @Override
    public void insertToCreateResultClassOne(ClassOneResultTable result) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(result);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }
    }

    @Override
    public void insertToCreateResultClassTwo(ClassTwoResultTable result) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(result);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }
    }

    @Override
    public void insertToCreateResultClassThree(ClassThreeResultTable result) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(result);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }
    }

    @Override
    public void insertToCreateResultClassFour(ClassFourResultTable result) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(result);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }
    }

    @Override
    public void insertToCreateResultClassFive(ClassFiveResultTable result) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(result);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }
    }

    @Override
    public void insertToCreateResultClassSix(ClassSixResultTable result) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(result);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }
    }

    @Override
    public void insertToCreateResultClassSeven(ClassSevenResultTable result) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(result);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }
    }

    @Override
    public void insertToCreateResultClassEight(ClassEightResultTable result) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(result);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }
    }

    @Override
    public void insertToCreateResultClassNine(ClassNineResultTable result) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(result);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }
    }

    @Override
    public void insertToCreateResultClassTen(ClassTenResultTable result) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(result);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }
    }
    //End of Inseting for creating Result

    //Start of Querying from ResultTables To get Result
    @Override
    public List<ClassOneResultTable> findAllResultClassOne(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassOneResultTable where classOneAdmissionTable=:a");
        ClassOneAdmissionTable ad = new ClassOneAdmissionTable();
        ad.setRollNumberOne(roll);
        q.setParameter("a", ad);
        List<ClassOneResultTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassTwoResultTable> findAllResultClassTwo(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassTwoResultTable where classTwoAdmissionTable=:a");
        ClassTwoAdmissionTable ad = new ClassTwoAdmissionTable();
        ad.setRollNumberTwo(roll);
        q.setParameter("a", ad);
        List<ClassTwoResultTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassThreeResultTable> findAllResultClassThree(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassThreeResultTable where classThreeAdmissionTable=:a");
        ClassThreeAdmissionTable ad = new ClassThreeAdmissionTable();
        ad.setRollNumberThree(roll);
        q.setParameter("a", ad);
        List<ClassThreeResultTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassFourResultTable> findAllResultClassFour(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFourResultTable where classFourAdmissionTable=:a");
        ClassFourAdmissionTable ad = new ClassFourAdmissionTable();
        ad.setRollNumberFour(roll);
        q.setParameter("a", ad);
        List<ClassFourResultTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassFiveResultTable> findAllResultClassFive(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFiveResultTable where classFiveAdmissionTable=:a");
        ClassFiveAdmissionTable ad = new ClassFiveAdmissionTable();
        ad.setRollNumberFive(roll);
        q.setParameter("a", ad);
        List<ClassFiveResultTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassSixResultTable> findAllResultClassSix(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSixResultTable where classSixAdmissionTable=:a");
        ClassSixAdmissionTable ad = new ClassSixAdmissionTable();
        ad.setRollNumberSix(roll);
        q.setParameter("a", ad);
        List<ClassSixResultTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassSevenResultTable> findAllResultClassSeven(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSevenResultTable where classSevenAdmissionTable=:a");
        ClassSevenAdmissionTable ad = new ClassSevenAdmissionTable();
        ad.setRollNumberSeven(roll);
        q.setParameter("a", ad);
        List<ClassSevenResultTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassEightResultTable> findAllResultClassEight(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassEightResultTable where classEightAdmissionTable=:a");
        ClassEightAdmissionTable ad = new ClassEightAdmissionTable();
        ad.setRollNumberEight(roll);
        q.setParameter("a", ad);
        List<ClassEightResultTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassNineResultTable> findAllResultClassNine(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassNineResultTable where classNineAdmissionTable=:a");
        ClassNineAdmissionTable ad = new ClassNineAdmissionTable();
        ad.setRollNumberNine(roll);
        q.setParameter("a", ad);
        List<ClassNineResultTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassTenResultTable> findAllResultClassTen(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassTenResultTable where classTenStudentInformationTable=:a");
        ClassTenStudentInformationTable ad = new ClassTenStudentInformationTable();
        ad.setRollNumberTen(roll);
        q.setParameter("a", ad);
        List<ClassTenResultTable> list = q.list();
        return list;
    }
    //End of Querying from ResultTables To get Result

    @Override
    public boolean insertToAddNewTeacher(StuffInformationTable stuff) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(stuff);
            s.getTransaction().commit();
            return true;

        } catch (Exception e) {
            s.getTransaction().rollback();
            return false;
        }
    }

    @Override
    public List<ClassOneAttendanceTable> findAttendanceClassOne() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassOneAttendanceTable> list = s.createCriteria(ClassOneAttendanceTable.class).list();
        return list;
    }

    @Override
    public boolean insertToAttendanceTableClassOne(List<ClassOneAttendanceTable> attList) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();

            for (int i = 0; i < attList.size(); i++) {
                ClassOneAttendanceTable att = attList.get(i);
                s.save(att);
            }

            s.getTransaction().commit();
            return true;

        } catch (Exception e) {
            s.getTransaction().rollback();
            return false;
        }
    }

    public List<ClassOneAdmissionTable> findAllIdAttendanceClassOne() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("select rollNumberOne from ClassOneAdmissionTable");
        List<ClassOneAdmissionTable> list = q.list();
        return list;
    }

    //Show Information
    @Override
    public List<ClassOneAdmissionTable> findAllStudentInfoClassOne() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassOneAdmissionTable> list = s.createCriteria(ClassOneAdmissionTable.class).list();
        return list;
    }

    @Override
    public List<ClassTwoAdmissionTable> findAllStudentInfoClassTwo() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassTwoAdmissionTable> list = s.createCriteria(ClassTwoAdmissionTable.class).list();
        return list;
    }

    @Override
    public List<ClassThreeAdmissionTable> findAllStudentInfoClassThree() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassThreeAdmissionTable> list = s.createCriteria(ClassThreeAdmissionTable.class).list();
        return list;
    }

    @Override
    public List<ClassFourAdmissionTable> findAllStudentInfoClassFour() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassFourAdmissionTable> list = s.createCriteria(ClassFourAdmissionTable.class).list();
        return list;
    }

    @Override
    public List<ClassFiveAdmissionTable> findAllStudentInfoClassFive() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassFiveAdmissionTable> list = s.createCriteria(ClassFiveAdmissionTable.class).list();
        return list;
    }

    @Override
    public List<ClassSixAdmissionTable> findAllStudentInfoClassSix() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassSixAdmissionTable> list = s.createCriteria(ClassSixAdmissionTable.class).list();
        return list;
    }

    @Override
    public List<ClassSevenAdmissionTable> findAllStudentInfoClassSeven() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassSevenAdmissionTable> list = s.createCriteria(ClassSevenAdmissionTable.class).list();
        return list;
    }

    @Override
    public List<ClassEightAdmissionTable> findAllStudentInfoClassEight() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassEightAdmissionTable> list = s.createCriteria(ClassEightAdmissionTable.class).list();
        return list;
    }

    @Override
    public List<ClassNineAdmissionTable> findAllStudentInfoClassNine() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassNineAdmissionTable> list = s.createCriteria(ClassNineAdmissionTable.class).list();
        return list;
    }

    @Override
    public List<ClassTenStudentInformationTable> findAllStudentInfoClassTen() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<ClassTenStudentInformationTable> list = s.createCriteria(ClassTenStudentInformationTable.class).list();
        return list;
    }

    //Get Information from Payment table
    @Override
    public List<ClassOnePaymentTable> findOneStudentPayementClassOne(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassOnePaymentTable where classOneAdmissionTable=:a");
        ClassOneAdmissionTable adm = new ClassOneAdmissionTable();
        adm.setRollNumberOne(roll);
        q.setParameter("a", adm);
        List<ClassOnePaymentTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassTwoPaymentTable> findOneStudentPayementClassTwo(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassTwoPaymentTable where classTwoAdmissionTable=:a");
        ClassTwoAdmissionTable adm = new ClassTwoAdmissionTable();
        adm.setRollNumberTwo(roll);
        q.setParameter("a", adm);
        List<ClassTwoPaymentTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassThreePaymentTable> findOneStudentPayementClassThree(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassThreePaymentTable where classThreeAdmissionTable=:a");
        ClassThreeAdmissionTable adm = new ClassThreeAdmissionTable();
        adm.setRollNumberThree(roll);
        q.setParameter("a", adm);
        List<ClassThreePaymentTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassFourPaymentTable> findOneStudentPayementClassFour(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFourPaymentTable where classFourAdmissionTable=:a");
        ClassFourAdmissionTable adm = new ClassFourAdmissionTable();
        adm.setRollNumberFour(roll);
        q.setParameter("a", adm);
        List<ClassFourPaymentTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassFivePaymentTable> findOneStudentPayementClassFive(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFivePaymentTable where classFiveAdmissionTable=:a");
        ClassFiveAdmissionTable adm = new ClassFiveAdmissionTable();
        adm.setRollNumberFive(roll);
        q.setParameter("a", adm);
        List<ClassFivePaymentTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassSixPaymentTable> findOneStudentPayementClassSix(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSixPaymentTable where classSixAdmissionTable=:a");
        ClassSixAdmissionTable adm = new ClassSixAdmissionTable();
        adm.setRollNumberSix(roll);
        q.setParameter("a", adm);
        List<ClassSixPaymentTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassSevenPaymentTable> findOneStudentPayementClassSeven(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSevenPaymentTable where classSevenAdmissionTable=:a");
        ClassSevenAdmissionTable adm = new ClassSevenAdmissionTable();
        adm.setRollNumberSeven(roll);
        q.setParameter("a", adm);
        List<ClassSevenPaymentTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassEightPaymentTable> findOneStudentPayementClassEight(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassEightPaymentTable where classEightAdmissionTable=:a");
        ClassEightAdmissionTable adm = new ClassEightAdmissionTable();
        adm.setRollNumberEight(roll);
        q.setParameter("a", adm);
        List<ClassEightPaymentTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassNinePaymentTable> findOneStudentPayementClassNine(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassNinePaymentTable where classNineAdmissionTable=:a");
        ClassNineAdmissionTable adm = new ClassNineAdmissionTable();
        adm.setRollNumberNine(roll);
        q.setParameter("a", adm);
        List<ClassNinePaymentTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassTenPaymentTable> findOneStudentPayementClassTen(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassOnePaymentTable where classTenStudentInformationTable=:a");
        ClassTenStudentInformationTable adm = new ClassTenStudentInformationTable();
        adm.setRollNumberTen(roll);
        q.setParameter("a", adm);
        List<ClassTenPaymentTable> list = q.list();
        return list;
    }

    //End Get Information from Payment table
    @Override
    public void insertToPaymentTableClassOne(ClassOnePaymentTable adPayment) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adPayment);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToPaymentTableClassTwo(ClassTwoPaymentTable adPayment) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adPayment);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToPaymentTableClassThree(ClassThreePaymentTable adPayment) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adPayment);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToPaymentTableClassFour(ClassFourPaymentTable adPayment) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adPayment);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToPaymentTableClassFive(ClassFivePaymentTable adPayment) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adPayment);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToPaymentTableClassSix(ClassSixPaymentTable adPayment) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adPayment);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToPaymentTableClassSeven(ClassSevenPaymentTable adPayment) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adPayment);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToPaymentTableClassEight(ClassEightPaymentTable adPayment) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adPayment);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToPaymentTableClassNine(ClassNinePaymentTable adPayment) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adPayment);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToPaymentTableClassTen(ClassTenPaymentTable adPayment) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adPayment);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    //Get Information from Attendace table
    @Override
    public List<ClassOneAttendanceTable> findOneStudentAttendanceClassOne(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassOneAttendanceTable where classOneAdmissionTable=:a");
        ClassOneAdmissionTable adm = new ClassOneAdmissionTable();
        adm.setRollNumberOne(roll);
        q.setParameter("a", adm);
        List<ClassOneAttendanceTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassTwoAttendanceTable> findOneStudentAttendanceClassTwo(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassTwoAttendanceTable where classTwoAdmissionTable=:a");
        ClassTwoAdmissionTable adm = new ClassTwoAdmissionTable();
        adm.setRollNumberTwo(roll);
        q.setParameter("a", adm);
        List<ClassTwoAttendanceTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassThreeAttendanceTable> findOneStudentAttendanceClassThree(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassThreeAttendanceTable where classThreeAdmissionTable=:a");
        ClassThreeAdmissionTable adm = new ClassThreeAdmissionTable();
        adm.setRollNumberThree(roll);
        q.setParameter("a", adm);
        List<ClassThreeAttendanceTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassFourAttendanceTable> findOneStudentAttendanceClassFour(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFourAttendanceTable where classFourAdmissionTable=:a");
        ClassFourAdmissionTable adm = new ClassFourAdmissionTable();
        adm.setRollNumberFour(roll);
        q.setParameter("a", adm);
        List<ClassFourAttendanceTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassFiveAttendanceTable> findOneStudentAttendanceClassFive(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFiveAttendanceTable where classFiveAdmissionTable=:a");
        ClassFiveAdmissionTable adm = new ClassFiveAdmissionTable();
        adm.setRollNumberFive(roll);
        q.setParameter("a", adm);
        List<ClassFiveAttendanceTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassSixAttendanceTable> findOneStudentAttendanceClassSix(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSixAttendanceTable where classSixAdmissionTable=:a");
        ClassSixAdmissionTable adm = new ClassSixAdmissionTable();
        adm.setRollNumberSix(roll);
        q.setParameter("a", adm);
        List<ClassSixAttendanceTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassSevenAttendanceTable> findOneStudentAttendanceClassSeven(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSevenAttendanceTable where classSevenAdmissionTable=:a");
        ClassSevenAdmissionTable adm = new ClassSevenAdmissionTable();
        adm.setRollNumberSeven(roll);
        q.setParameter("a", adm);
        List<ClassSevenAttendanceTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassEightAttendanceTable> findOneStudentAttendanceClassEight(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassEightAttendanceTable where classEightAdmissionTable=:a");
        ClassEightAdmissionTable adm = new ClassEightAdmissionTable();
        adm.setRollNumberEight(roll);
        q.setParameter("a", adm);
        List<ClassEightAttendanceTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassNineAttendanceTable> findOneStudentAttendanceClassNine(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassNineAttendanceTable where classNineAdmissionTable=:a");
        ClassNineAdmissionTable adm = new ClassNineAdmissionTable();
        adm.setRollNumberNine(roll);
        q.setParameter("a", adm);
        List<ClassNineAttendanceTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassTenAttendanceTable> findOneStudentAttendanceClassTen(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassTenAttendanceTable where classTenStudentInformationTable=:a");
        ClassTenStudentInformationTable adm = new ClassTenStudentInformationTable();
        adm.setRollNumberTen(roll);
        q.setParameter("a", adm);
        List<ClassTenAttendanceTable> list = q.list();
        return list;
    }

    //Insert into Attendance Table
    @Override
    public void insertToAttendanceTableClassOne(ClassOneAttendanceTable addAttnd) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(addAttnd);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToAttendanceTableClassTwo(ClassTwoAttendanceTable addAttnd) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(addAttnd);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToAttendanceTableClassThree(ClassThreeAttendanceTable addAttnd) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(addAttnd);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToAttendanceTableClassFour(ClassFourAttendanceTable addAttnd) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(addAttnd);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToAttendanceTableClassFive(ClassFiveAttendanceTable addAttnd) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(addAttnd);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToAttendanceTableClassSix(ClassSixAttendanceTable addAttnd) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(addAttnd);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToAttendanceTableClassSeven(ClassSevenAttendanceTable addAttnd) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(addAttnd);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToAttendanceTableClassEight(ClassEightAttendanceTable addAttnd) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(addAttnd);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToAttendanceTableClassNine(ClassNineAttendanceTable addAttnd) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(addAttnd);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void insertToAttendanceTableClassTen(ClassTenAttendanceTable addAttnd) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(addAttnd);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    //Getting Data from Admission Table for Edit
    @Override
    public List<ClassOneAdmissionTable> findOneStudentForEdit(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassOneAdmissionTable where rollNumberOne=:a");
        q.setParameter("a", roll);
        List<ClassOneAdmissionTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassTwoAdmissionTable> findTwoStudentForEdit(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassTwoAdmissionTable where rollNumberTwo=:a");
        q.setParameter("a", roll);
        List<ClassTwoAdmissionTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassThreeAdmissionTable> findThreeStudentForEdit(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassThreeAdmissionTable where rollNumberThree=:a");
        q.setParameter("a", roll);
        List<ClassThreeAdmissionTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassFourAdmissionTable> findFourStudentForEdit(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFourAdmissionTable where rollNumberFour=:a");
        q.setParameter("a", roll);
        List<ClassFourAdmissionTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassFiveAdmissionTable> findFiveStudentForEdit(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassFiveAdmissionTable where rollNumberFive=:a");
        q.setParameter("a", roll);
        List<ClassFiveAdmissionTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassSixAdmissionTable> findSixStudentForEdit(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSixAdmissionTable where rollNumberSix=:a");
        q.setParameter("a", roll);
        List<ClassSixAdmissionTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassSevenAdmissionTable> findSevenStudentForEdit(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassSevenAdmissionTable where rollNumberSeven=:a");
        q.setParameter("a", roll);
        List<ClassSevenAdmissionTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassEightAdmissionTable> findEightStudentForEdit(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassEightAdmissionTable where rollNumberEight=:a");
        q.setParameter("a", roll);
        List<ClassEightAdmissionTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassNineAdmissionTable> findNineStudentForEdit(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassNineAdmissionTable where rollNumberNine=:a");
        q.setParameter("a", roll);
        List<ClassNineAdmissionTable> list = q.list();
        return list;
    }

    @Override
    public List<ClassTenStudentInformationTable> findTenStudentForEdit(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from ClassTenStudentInformationTable where rollNumberTen=:a");
        q.setParameter("a", roll);
        List<ClassTenStudentInformationTable> list = q.list();
        return list;
    }

    //Statrt Update of Admission Table
    @Override
    public void updateInformationClassOne(ClassOneAdmissionTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.update(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void updateInformationClassTwo(ClassTwoAdmissionTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.update(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void updateInformationClassThree(ClassThreeAdmissionTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.update(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void updateInformationClassFour(ClassFourAdmissionTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.update(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void updateInformationClassFive(ClassFiveAdmissionTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.update(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void updateInformationClassSix(ClassSixAdmissionTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.update(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void updateInformationClassSeven(ClassSevenAdmissionTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.update(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void updateInformationClassEight(ClassEightAdmissionTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.update(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void updateInformationClassNine(ClassNineAdmissionTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.update(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    @Override
    public void updateInformationClassTen(ClassTenStudentInformationTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.update(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

    //Delete Student from Admission Table
    @Override
    public void deleteStudentClassOne(ClassOneAdmissionTable addTable) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.delete(addTable);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }
    
    //Find Status
    @Override
    public List<ClassOneAttendanceTable> findStudentStatusClassOne(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassOneAttendanceTable WHERE classOneAdmissionTable=:obj");
        ClassOneAdmissionTable ad = new ClassOneAdmissionTable();
        ad.setRollNumberOne(roll);
        //Query q = s.createQuery("FROM ClassOneAttendanceTable AS c WHERE c.date BETWEEN :stDate AND :edDate ");
        q.setParameter("obj", ad);
        
        List<ClassOneAttendanceTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassTwoAttendanceTable> findStudentStatusClassTwo(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassTwoAttendanceTable WHERE classTwoAdmissionTable=:obj");
        ClassTwoAdmissionTable ad = new ClassTwoAdmissionTable();
        ad.setRollNumberTwo(roll);
        //Query q = s.createQuery("FROM ClassOneAttendanceTable AS c WHERE c.date BETWEEN :stDate AND :edDate ");
        q.setParameter("obj", ad);
        
        List<ClassTwoAttendanceTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassThreeAttendanceTable> findStudentStatusClassThree(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassThreeAttendanceTable WHERE classThreeAdmissionTable=:obj");
        ClassThreeAdmissionTable ad = new ClassThreeAdmissionTable();
        ad.setRollNumberThree(roll);
        //Query q = s.createQuery("FROM ClassOneAttendanceTable AS c WHERE c.date BETWEEN :stDate AND :edDate ");
        q.setParameter("obj", ad);
        
        List<ClassThreeAttendanceTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassFourAttendanceTable> findStudentStatusClassFour(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassFourAttendanceTable WHERE classFourAdmissionTable=:obj");
        ClassFourAdmissionTable ad = new ClassFourAdmissionTable();
        ad.setRollNumberFour(roll);
        //Query q = s.createQuery("FROM ClassOneAttendanceTable AS c WHERE c.date BETWEEN :stDate AND :edDate ");
        q.setParameter("obj", ad);
        
        List<ClassFourAttendanceTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassFiveAttendanceTable> findStudentStatusClassFive(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassFiveAttendanceTable WHERE classFiveAdmissionTable=:obj");
        ClassFiveAdmissionTable ad = new ClassFiveAdmissionTable();
        ad.setRollNumberFive(roll);
        //Query q = s.createQuery("FROM ClassOneAttendanceTable AS c WHERE c.date BETWEEN :stDate AND :edDate ");
        q.setParameter("obj", ad);
        
        List<ClassFiveAttendanceTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassSixAttendanceTable> findStudentStatusClassSix(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassSixAttendanceTable WHERE classSixAdmissionTable=:obj");
        ClassSixAdmissionTable ad = new ClassSixAdmissionTable();
        ad.setRollNumberSix(roll);
        //Query q = s.createQuery("FROM ClassOneAttendanceTable AS c WHERE c.date BETWEEN :stDate AND :edDate ");
        q.setParameter("obj", ad);
        
        List<ClassSixAttendanceTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassSevenAttendanceTable> findStudentStatusClassSeven(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassSevenAttendanceTable WHERE classSevenAdmissionTable=:obj");
        ClassSevenAdmissionTable ad = new ClassSevenAdmissionTable();
        ad.setRollNumberSeven(roll);
        //Query q = s.createQuery("FROM ClassOneAttendanceTable AS c WHERE c.date BETWEEN :stDate AND :edDate ");
        q.setParameter("obj", ad);
        
        List<ClassSevenAttendanceTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassEightAttendanceTable> findStudentStatusClassEight(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassEightAttendanceTable WHERE classEightAdmissionTable=:obj");
        ClassEightAdmissionTable ad = new ClassEightAdmissionTable();
        ad.setRollNumberEight(roll);
        //Query q = s.createQuery("FROM ClassOneAttendanceTable AS c WHERE c.date BETWEEN :stDate AND :edDate ");
        q.setParameter("obj", ad);
        
        List<ClassEightAttendanceTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassNineAttendanceTable> findStudentStatusClassNine(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassNineAttendanceTable WHERE classNineAdmissionTable=:obj");
        ClassNineAdmissionTable ad = new ClassNineAdmissionTable();
        ad.setRollNumberNine(roll);
        //Query q = s.createQuery("FROM ClassOneAttendanceTable AS c WHERE c.date BETWEEN :stDate AND :edDate ");
        q.setParameter("obj", ad);
        
        List<ClassNineAttendanceTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassTenAttendanceTable> findStudentStatusClassTen(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassTenAttendanceTable WHERE classTenStudentInformationTable=:obj");
        ClassTenStudentInformationTable ad = new ClassTenStudentInformationTable();
        ad.setRollNumberTen(roll);
        //Query q = s.createQuery("FROM ClassOneAttendanceTable AS c WHERE c.date BETWEEN :stDate AND :edDate ");
        q.setParameter("obj", ad);
        
        List<ClassTenAttendanceTable> list = q.list();
        return list;
    }
    
    
    //Find Payment Status
    @Override
    public List<ClassOnePaymentTable> findPaymentStatusClassOne(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassOnePaymentTable WHERE classOneAdmissionTable=:obj");
        ClassOneAdmissionTable ad = new ClassOneAdmissionTable();
        ad.setRollNumberOne(roll);
        q.setParameter("obj", ad);
        List<ClassOnePaymentTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassTwoPaymentTable> findPaymentStatusClassTwo(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassTwoPaymentTable WHERE classTwoAdmissionTable=:obj");
        ClassTwoAdmissionTable ad = new ClassTwoAdmissionTable();
        ad.setRollNumberTwo(roll);
        q.setParameter("obj", ad);
        List<ClassTwoPaymentTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassThreePaymentTable> findPaymentStatusClassThree(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassThreePaymentTable WHERE classThreeAdmissionTable=:obj");
        ClassThreeAdmissionTable ad = new ClassThreeAdmissionTable();
        ad.setRollNumberThree(roll);
        q.setParameter("obj", ad);
        List<ClassThreePaymentTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassFourPaymentTable> findPaymentStatusClassFour(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassOnePaymentTable WHERE classOneAdmissionTable=:obj");
        ClassFourAdmissionTable ad = new ClassFourAdmissionTable();
        ad.setRollNumberFour(roll);
        q.setParameter("obj", ad);
        List<ClassFourPaymentTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassFivePaymentTable> findPaymentStatusClassFive(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassFivePaymentTable WHERE classFiveAdmissionTable=:obj");
        ClassFiveAdmissionTable ad = new ClassFiveAdmissionTable();
        ad.setRollNumberFive(roll);
        q.setParameter("obj", ad);
        List<ClassFivePaymentTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassSixPaymentTable> findPaymentStatusClassSix(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassSixPaymentTable WHERE classSixAdmissionTable=:obj");
        ClassSixAdmissionTable ad = new ClassSixAdmissionTable();
        ad.setRollNumberSix(roll);
        q.setParameter("obj", ad);
        List<ClassSixPaymentTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassSevenPaymentTable> findPaymentStatusClassSeven(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassSevenPaymentTable WHERE classSevenAdmissionTable=:obj");
        ClassSevenAdmissionTable ad = new ClassSevenAdmissionTable();
        ad.setRollNumberSeven(roll);
        q.setParameter("obj", ad);
        List<ClassSevenPaymentTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassEightPaymentTable> findPaymentStatusClassEight(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassEightPaymentTable WHERE classEightAdmissionTable=:obj");
        ClassEightAdmissionTable ad = new ClassEightAdmissionTable();
        ad.setRollNumberEight(roll);
        q.setParameter("obj", ad);
        List<ClassEightPaymentTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassNinePaymentTable> findPaymentStatusClassNine(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassNinePaymentTable WHERE classNineAdmissionTable=:obj");
        ClassNineAdmissionTable ad = new ClassNineAdmissionTable();
        ad.setRollNumberNine(roll);
        q.setParameter("obj", ad);
        List<ClassNinePaymentTable> list = q.list();
        return list;
    }
    @Override
    public List<ClassTenPaymentTable> findPaymentStatusClassTen(int roll) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("FROM ClassTenPaymentTable WHERE classTenStudentInformationTable=:obj");
        ClassTenStudentInformationTable ad = new ClassTenStudentInformationTable();
        ad.setRollNumberTen(roll);
        q.setParameter("obj", ad);
        List<ClassTenPaymentTable> list = q.list();
        return list;
    }
    
    
    @Override
    public List<StuffInformationTable> findAllTeachers() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<StuffInformationTable> list = s.createCriteria(StuffInformationTable.class).list();
        s.getTransaction().commit();
        return list;

    }
    
//     @Override
//    public void insertForRegisterAdmin(AdminLoginTable loginTable) {
//        Session s = HibernateUtil.getSessionFactory().openSession();
//        try {
//            s.beginTransaction();
//            s.save(loginTable);
//            s.getTransaction().commit();
//            s.close();
//        } catch (Exception e) {
//            s.getTransaction().rollback();
//            s.close();
//        }
//
//    }
    
    
     @Override
    public void createSignup(AdminLoginTable adLg) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        try {
            s.beginTransaction();
            s.save(adLg);
            s.getTransaction().commit();
            s.close();
        } catch (Exception e) {
            s.getTransaction().rollback();
            s.close();
        }

    }

}

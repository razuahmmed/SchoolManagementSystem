/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam;

import com.entity.AdminLoginTable;
import com.entity.ClassEightAdmissionTable;
import com.entity.ClassEightAdmissionTestTable;
import com.entity.ClassEightAttendanceTable;
import com.entity.ClassEightPaymentTable;
import com.entity.ClassEightResultTable;
import com.entity.ClassFiveAdmissionTable;
import com.entity.ClassFiveAdmissionTestTable;
import com.entity.ClassFiveAttendanceTable;
import com.entity.ClassFivePaymentTable;
import com.entity.ClassFiveResultTable;
import com.entity.ClassFourAdmissionTable;
import com.entity.ClassFourAdmissionTestTable;
import com.entity.ClassFourAttendanceTable;
import com.entity.ClassFourPaymentTable;
import com.entity.ClassFourResultTable;
import com.entity.ClassNineAdmissionTable;
import com.entity.ClassNineAdmissionTestTable;
import com.entity.ClassNineAttendanceTable;
import com.entity.ClassNinePaymentTable;
import com.entity.ClassNineResultTable;
import com.entity.ClassOneAdmissionTable;
import com.entity.ClassOneAdmissionTestTable;
import com.entity.ClassOneAttendanceTable;
import com.entity.ClassOnePaymentTable;
import com.entity.ClassOneResultTable;
import com.entity.ClassSevenAdmissionTable;
import com.entity.ClassSevenAdmissionTestTable;
import com.entity.ClassSevenAttendanceTable;
import com.entity.ClassSevenPaymentTable;
import com.entity.ClassSevenResultTable;
import com.entity.ClassSixAdmissionTable;
import com.entity.ClassSixAdmissionTestTable;
import com.entity.ClassSixAttendanceTable;
import com.entity.ClassSixPaymentTable;
import com.entity.ClassSixResultTable;
import com.entity.ClassTenAttendanceTable;
import com.entity.ClassTenPaymentTable;
import com.entity.ClassTenResultTable;
import com.entity.ClassTenStudentInformationTable;
import com.entity.ClassThreeAdmissionTable;
import com.entity.ClassThreeAdmissionTestTable;
import com.entity.ClassThreeAttendanceTable;
import com.entity.ClassThreePaymentTable;
import com.entity.ClassThreeResultTable;
import com.entity.ClassTwoAdmissionTable;
import com.entity.ClassTwoAdmissionTestTable;
import com.entity.ClassTwoAttendanceTable;
import com.entity.ClassTwoPaymentTable;
import com.entity.ClassTwoResultTable;
import com.entity.StuffInformationTable;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Maksud Rahaman
 */
public interface ModelLayer {

    void insertClassOneAdmissionTest(ClassOneAdmissionTestTable adTestOne);

    void insertClassTwoAdmissionTest(ClassTwoAdmissionTestTable adTestTwo);

    void insertClassThreeAdmissionTest(ClassThreeAdmissionTestTable adTestThree);

    void insertClassFourAdmissionTest(ClassFourAdmissionTestTable adTestFour);

    void insertClassFiveAdmissionTest(ClassFiveAdmissionTestTable adTestFive);

    void insertClassSixAdmissionTest(ClassSixAdmissionTestTable adTestSix);

    void insertClassSevenAdmissionTest(ClassSevenAdmissionTestTable adTestSeven);

    void insertClassEightAdmissionTest(ClassEightAdmissionTestTable adTestEight);

    void insertClassNineAdmissionTest(ClassNineAdmissionTestTable adTestNine);

    public List<ClassOneAdmissionTestTable> findAdmitCardClassOne(String name, String email);

    public List<ClassTwoAdmissionTestTable> findAdmitCardClassTwo(String name, String email);

    public List<ClassThreeAdmissionTestTable> findAdmitCardClassThree(String name, String email);

    public List<ClassFourAdmissionTestTable> findAdmitCardClassFour(String name, String email);

    public List<ClassFiveAdmissionTestTable> findAdmitCardClassFive(String name, String email);

    public List<ClassSixAdmissionTestTable> findAdmitCardClassSix(String name, String email);

    public List<ClassSevenAdmissionTestTable> findAdmitCardClassSeven(String name, String email);

    public List<ClassEightAdmissionTestTable> findAdmitCardClassEight(String name, String email);

    public List<ClassNineAdmissionTestTable> findAdmitCardClassNine(String name, String email);

    public List<ClassOneAdmissionTestTable> confirmAdmissionClassOne(int serialNumber, String email);

    public List<ClassTwoAdmissionTestTable> confirmAdmissionClassTwo(int serialNumber, String email);

    public List<ClassThreeAdmissionTestTable> confirmAdmissionClassThree(int serialNumber, String email);

    public List<ClassFourAdmissionTestTable> confirmAdmissionClassFour(int serialNumber, String email);

    public List<ClassFiveAdmissionTestTable> confirmAdmissionClassFive(int serialNumber, String email);

    public List<ClassSixAdmissionTestTable> confirmAdmissionClassSix(int serialNumber, String email);

    public List<ClassSevenAdmissionTestTable> confirmAdmissionClassSeven(int serialNumber, String email);

    public List<ClassEightAdmissionTestTable> confirmAdmissionClassEight(int serialNumber, String email);

    public List<ClassNineAdmissionTestTable> confirmAdmissionClassNine(int serialNumber, String email);

    public void insertClassOneAdmission(ClassOneAdmissionTable adOne);

    public void insertClassTwoAdmission(ClassTwoAdmissionTable adTwo);

    public void insertClassThreeAdmission(ClassThreeAdmissionTable adThree);

    public void insertClassFourAdmission(ClassFourAdmissionTable adFour);

    public void insertClassFiveAdmission(ClassFiveAdmissionTable adFive);

    public void insertClassSixAdmission(ClassSixAdmissionTable adSix);

    public void insertClassSevenAdmission(ClassSevenAdmissionTable adSeven);

    public void insertClassEightAdmission(ClassEightAdmissionTable adEight);

    public void insertClassNineAdmission(ClassNineAdmissionTable adNine);

    public List<ClassOneAdmissionTable> findAdmissionInformationForRollNumberClassOne(int serialNumber);

    public List<ClassTwoAdmissionTable> findAdmissionInformationForRollNumberClassTwo(int serialNumber);

    public List<ClassThreeAdmissionTable> findAdmissionInformationForRollNumberClassThree(int serialNumber);

    public List<ClassFourAdmissionTable> findAdmissionInformationForRollNumberClassFour(int serialNumber);

    public List<ClassFiveAdmissionTable> findAdmissionInformationForRollNumberClassFive(int serialNumber);

    public List<ClassSixAdmissionTable> findAdmissionInformationForRollNumberClassSix(int serialNumber);

    public List<ClassSevenAdmissionTable> findAdmissionInformationForRollNumberClassSeven(int serialNumber);

    public List<ClassEightAdmissionTable> findAdmissionInformationForRollNumberClassEight(int serialNumber);

    public List<ClassNineAdmissionTable> findAdmissionInformationForRollNumberClassNine(int serialNumber);
    
    public List<ClassOneAdmissionTable> findAllInformationForFeesPaymentClassOne();
    public List<ClassTwoAdmissionTable> findAllInformationForFeesPaymentClassTwo();
    public List<ClassThreeAdmissionTable> findAllInformationForFeesPaymentClassThree();
    public List<ClassFourAdmissionTable> findAllInformationForFeesPaymentClassFour();
    public List<ClassFiveAdmissionTable> findAllInformationForFeesPaymentClassFive();
    public List<ClassSixAdmissionTable> findAllInformationForFeesPaymentClassSix();
    public List<ClassSevenAdmissionTable> findAllInformationForFeesPaymentClassSeven();
    public List<ClassEightAdmissionTable> findAllInformationForFeesPaymentClassEight();
    public List<ClassNineAdmissionTable> findAllInformationForFeesPaymentClassNine();
    public List<ClassTenStudentInformationTable> findAllInformationForFeesPaymentClassTen();
    
    
    public void insertToCreateResultClassOne(ClassOneResultTable result);
    public void insertToCreateResultClassTwo(ClassTwoResultTable result);
    public void insertToCreateResultClassThree(ClassThreeResultTable result);
    public void insertToCreateResultClassFour(ClassFourResultTable result);
    public void insertToCreateResultClassFive(ClassFiveResultTable result);
     public void insertToCreateResultClassSix(ClassSixResultTable result);
     public void insertToCreateResultClassSeven(ClassSevenResultTable result);
     public void insertToCreateResultClassEight(ClassEightResultTable result);
     public void insertToCreateResultClassNine(ClassNineResultTable result);
     public void insertToCreateResultClassTen(ClassTenResultTable result);
     
     
     public List<ClassOneResultTable> findAllResultClassOne(int roll);
     public List<ClassTwoResultTable> findAllResultClassTwo(int roll);
     public List<ClassThreeResultTable> findAllResultClassThree(int roll);
     public List<ClassFourResultTable> findAllResultClassFour(int roll);
     public List<ClassFiveResultTable> findAllResultClassFive(int roll);
     public List<ClassSixResultTable> findAllResultClassSix(int roll);
     public List<ClassSevenResultTable> findAllResultClassSeven(int roll) ;
     public List<ClassEightResultTable> findAllResultClassEight(int roll);
     public List<ClassNineResultTable> findAllResultClassNine(int roll);
     public List<ClassTenResultTable> findAllResultClassTen(int roll);
     
     
     public boolean insertToAddNewTeacher(StuffInformationTable stuff);
    
    public List<ClassOneAttendanceTable> findAttendanceClassOne();
    public boolean insertToAttendanceTableClassOne(List<ClassOneAttendanceTable> attList);
    
    
    public void createSignup(AdminLoginTable adLg);
    public boolean adminLoginConfirm(String username, String email, String admincode);
    public List<ClassOneAdmissionTable> findAllIdAttendanceClassOne();
    
    public List<ClassOneAdmissionTable> findAllStudentInfoClassOne();
    public List<ClassTwoAdmissionTable> findAllStudentInfoClassTwo();
    public List<ClassThreeAdmissionTable> findAllStudentInfoClassThree();
    public List<ClassFourAdmissionTable> findAllStudentInfoClassFour();
     public List<ClassFiveAdmissionTable> findAllStudentInfoClassFive();
     public List<ClassSixAdmissionTable> findAllStudentInfoClassSix();
      public List<ClassSevenAdmissionTable> findAllStudentInfoClassSeven();
      public List<ClassEightAdmissionTable> findAllStudentInfoClassEight();
      public List<ClassNineAdmissionTable> findAllStudentInfoClassNine();
      public List<ClassTenStudentInformationTable> findAllStudentInfoClassTen();
      
      
      public List<ClassOnePaymentTable> findOneStudentPayementClassOne(int roll);
      public List<ClassTwoPaymentTable> findOneStudentPayementClassTwo(int roll);
      public List<ClassThreePaymentTable> findOneStudentPayementClassThree(int roll);
       public List<ClassFourPaymentTable> findOneStudentPayementClassFour(int roll);
       public List<ClassFivePaymentTable> findOneStudentPayementClassFive(int roll);
       public List<ClassSixPaymentTable> findOneStudentPayementClassSix(int roll);
       public List<ClassSevenPaymentTable> findOneStudentPayementClassSeven(int roll);
       public List<ClassEightPaymentTable> findOneStudentPayementClassEight(int roll);
       public List<ClassNinePaymentTable> findOneStudentPayementClassNine(int roll);
       public List<ClassTenPaymentTable> findOneStudentPayementClassTen(int roll);
       
       public void insertToPaymentTableClassOne(ClassOnePaymentTable adPayment);
       public void insertToPaymentTableClassTwo(ClassTwoPaymentTable adPayment);
       public void insertToPaymentTableClassThree(ClassThreePaymentTable adPayment);
       public void insertToPaymentTableClassFour(ClassFourPaymentTable adPayment);
        public void insertToPaymentTableClassFive(ClassFivePaymentTable adPayment);
        public void insertToPaymentTableClassSix(ClassSixPaymentTable adPayment);
        public void insertToPaymentTableClassSeven(ClassSevenPaymentTable adPayment);
        public void insertToPaymentTableClassEight(ClassEightPaymentTable adPayment);
        public void insertToPaymentTableClassNine(ClassNinePaymentTable adPayment);
        public void insertToPaymentTableClassTen(ClassTenPaymentTable adPayment);
        
        
         public List<ClassOneAttendanceTable> findOneStudentAttendanceClassOne(int roll);
         public List<ClassTwoAttendanceTable> findOneStudentAttendanceClassTwo(int roll);
         public List<ClassThreeAttendanceTable> findOneStudentAttendanceClassThree(int roll);
         public List<ClassFourAttendanceTable> findOneStudentAttendanceClassFour(int roll);
         public List<ClassFiveAttendanceTable> findOneStudentAttendanceClassFive(int roll);
         public List<ClassSixAttendanceTable> findOneStudentAttendanceClassSix(int roll);
         public List<ClassSevenAttendanceTable> findOneStudentAttendanceClassSeven(int roll);
         public List<ClassEightAttendanceTable> findOneStudentAttendanceClassEight(int roll);
         public List<ClassNineAttendanceTable> findOneStudentAttendanceClassNine(int roll);
          public List<ClassTenAttendanceTable> findOneStudentAttendanceClassTen(int roll);
          
          
          public void insertToAttendanceTableClassOne(ClassOneAttendanceTable addAttnd);
          public void insertToAttendanceTableClassTwo(ClassTwoAttendanceTable addAttnd);
          public void insertToAttendanceTableClassThree(ClassThreeAttendanceTable addAttnd);
          public void insertToAttendanceTableClassFour(ClassFourAttendanceTable addAttnd);
          public void insertToAttendanceTableClassFive(ClassFiveAttendanceTable addAttnd);
          public void insertToAttendanceTableClassSix(ClassSixAttendanceTable addAttnd);
          public void insertToAttendanceTableClassSeven(ClassSevenAttendanceTable addAttnd);
          public void insertToAttendanceTableClassEight(ClassEightAttendanceTable addAttnd);
          public void insertToAttendanceTableClassNine(ClassNineAttendanceTable addAttnd);
          public void insertToAttendanceTableClassTen(ClassTenAttendanceTable addAttnd);
          
          
          public List<ClassOneAdmissionTable> findOneStudentForEdit(int roll);
          public List<ClassTwoAdmissionTable> findTwoStudentForEdit(int roll);
          public List<ClassThreeAdmissionTable> findThreeStudentForEdit(int roll);
          public List<ClassFourAdmissionTable> findFourStudentForEdit(int roll);
          public List<ClassFiveAdmissionTable> findFiveStudentForEdit(int roll);
          public List<ClassSixAdmissionTable> findSixStudentForEdit(int roll);
          public List<ClassSevenAdmissionTable> findSevenStudentForEdit(int roll);
          public List<ClassEightAdmissionTable> findEightStudentForEdit(int roll);
          public List<ClassNineAdmissionTable> findNineStudentForEdit(int roll);
          public List<ClassTenStudentInformationTable> findTenStudentForEdit(int roll);
          
          
          
          public void updateInformationClassOne(ClassOneAdmissionTable addTable);
          public void updateInformationClassTwo(ClassTwoAdmissionTable addTable);
          public void updateInformationClassThree(ClassThreeAdmissionTable addTable);
          public void updateInformationClassFour(ClassFourAdmissionTable addTable);
          public void updateInformationClassFive(ClassFiveAdmissionTable addTable);
          public void updateInformationClassSix(ClassSixAdmissionTable addTable);
          public void updateInformationClassSeven(ClassSevenAdmissionTable addTable);
          public void updateInformationClassEight(ClassEightAdmissionTable addTable);
          public void updateInformationClassNine(ClassNineAdmissionTable addTable);
          public void updateInformationClassTen(ClassTenStudentInformationTable addTable);
          
          
          public void deleteStudentClassOne(ClassOneAdmissionTable addTable);
          
          
          
          
          public List<ClassOneAttendanceTable> findStudentStatusClassOne(int roll);
          public List<ClassTwoAttendanceTable> findStudentStatusClassTwo(int roll);
          public List<ClassThreeAttendanceTable> findStudentStatusClassThree(int roll);
          public List<ClassFourAttendanceTable> findStudentStatusClassFour(int roll);
          public List<ClassFiveAttendanceTable> findStudentStatusClassFive(int roll);
          public List<ClassSixAttendanceTable> findStudentStatusClassSix(int roll);
          public List<ClassSevenAttendanceTable> findStudentStatusClassSeven(int roll);
          public List<ClassEightAttendanceTable> findStudentStatusClassEight(int roll);
          public List<ClassNineAttendanceTable> findStudentStatusClassNine(int roll);
          public List<ClassTenAttendanceTable> findStudentStatusClassTen(int roll);
          
          public List<ClassOnePaymentTable> findPaymentStatusClassOne(int roll);
          public List<ClassTwoPaymentTable> findPaymentStatusClassTwo(int roll);
          public List<ClassThreePaymentTable> findPaymentStatusClassThree(int roll);
          public List<ClassFourPaymentTable> findPaymentStatusClassFour(int roll);
          public List<ClassFivePaymentTable> findPaymentStatusClassFive(int roll);
          public List<ClassSixPaymentTable> findPaymentStatusClassSix(int roll);
          public List<ClassSevenPaymentTable> findPaymentStatusClassSeven(int roll);
          public List<ClassEightPaymentTable> findPaymentStatusClassEight(int roll);
           public List<ClassNinePaymentTable> findPaymentStatusClassNine(int roll);
           public List<ClassTenPaymentTable> findPaymentStatusClassTen(int roll);
          
       
      
           public List<StuffInformationTable> findAllTeachers();
           //public void insertForRegisterAdmin(AdminLoginTable loginTable);

}

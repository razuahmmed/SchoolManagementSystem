<%-- 
    Document   : studentInformaitonClassSeven
    Created on : Oct 13, 2015, 11:34:38 PM
    Author     : Maksud Rahaman
--%>

<%@ include file="headerForAdmin.jsp" %>
<div class="container">
    <div class="row">
        <!--        <div class="col-sm-3 subDropdown">
                   
                </div>-->
        <div class="col-sm-6">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <fieldset>
                <legend>Class Seven Student Information</legend>
                <table class="table">
                    <th>
                    <tr>
                        <td>Student's Identity</td>
                        
                        <td>Student's Name</td>
                        
                        <td>Father's Name</td>
                        
                        <td>Mother's Name</td>
                        
                        <td>Date Of Birth</td>
                       
                        <td>Student's Address</td>
                       
                        <td>Religion</td>
                        
                        <td>Gender</td>
                        
                        <td>Student's Email</td>
                       
                        <td>Student's Phone</td>
                        
                        <td>Nationality</td>
                        
                        <td>Image</td>
                        
                        
                        <td>&nbsp;</td>
                        
                        
                        <td>&nbsp;</td>
                       
                        
                        <td>&nbsp;</td>
                        
                        
                        <td>&nbsp;</td>
                       
                    </tr>
                    </th>

                    <c:forEach var="list" items="${List}">
                        <tr>
                            <td>${list.rollNumberSeven}</td>
                            
                            <td>${list.studentName}</td>
                            
                            <td>${list.fatherName}</td>
                            
                            <td>${list.motherName}</td>
                            
                            <td>${list.dateOfBirth}</td>
                            
                            <td>${list.presentAddress}</td>
                            
                            <td>${list.religion}</td>
                            
                            <td>${list.gender}</td>
                           
                            <td>${list.email}</td>
                            
                            <td>${list.phone}</td>
                            
                            <td>${list.nationality}</td>
                            
                            <td><img width="50px" height="50px" src="<c:url value="${list.image}"></c:url>"></td>
                            
                            <td><a href="goToPayingFeesClassSeven.htm?rollNumberSeven=${list.rollNumberSeven}&studentName=${list.studentName}"> Fees</a></td>
                            
                            <td><a href="goToCreateAttendanceClassSeven.htm?rollNumberSeven=${list.rollNumberSeven}&studentName=${list.studentName}">Attendance</a></td>
                            
                            <td><a href="goToEditClassSeven.htm?rollNumberSeven=${list.rollNumberSeven}">Edit</a></td>
                            
                            <td><a href="doDeleteClassSeven.htm?rollNumberSeven=${list.rollNumberSeven}">Delete</a></td>
                           
                        </tr>
                    </c:forEach>
                </table>

            </fieldset>
        </div>

    </div>
</div>

<%@ include file="footerForAdmin.jsp" %>

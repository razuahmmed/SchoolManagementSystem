<%-- 
    Document   : admissionFormClassFour
    Created on : Oct 1, 2015, 4:26:22 PM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <form action="admissionForClassFour.htm">
                <fieldset>
                    <legend>Admission Form For Class Four</legend>
                    <legend>Fill up the Form with Correct Information</legend>
                    <table class="table">
                        <tr>
                            <td>Serial Number</td>
                            <td><input type="text" readonly="true" name="serialNumberFouradtest" value="${Student.serialNumberFouradtest}"/></td>
                        </tr>
                        <tr>
                            <td>Student's Name</td>
                            <td><input type="text" name="studentName" value="${Student.studentName}"/></td>
                            <td><img style="height: 50px; width: 100px" src="<c:url value="${Student.image}"></c:url>"></td>
                        </tr>
                        <tr>
                            <td>Father's Name</td>
                            <td><input type="text" name="fatherName" value="${Student.fatherName}"/></td>
                        </tr>
                        <tr>
                            <td>Mother's Name</td>
                            <td><input type="text" name="motherName" value="${Student.motherName}"/></td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td><input type="text" name="dateOfBirth" value="${Student.dateOfBirht}"/></td>
                        </tr>
                        <tr>
                            <td>Present Address</td>
                            <td><input type="text" name="presentAddress"/></td>
                        </tr>
                        <tr>
                            <td>Permanent Address</td>
                            <td><input type="text" name="permanentAddress"/></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>
                                <input type="radio" name="gender" value="Male"/>Male &nbsp;
                                <input type="radio" name="gender" value="Female"/>Female &nbsp;
                                <input type="radio" name="gender" value="Third"/>Third &nbsp;
                            </td>

                        </tr>
                        <tr>
                            <td>Religion</td>
                            <td>
                                <input type="radio" name="religion" value="Islam"/>Islam &nbsp;
                                <input type="radio" name="religion" value="Hinduism"/>Hinduism &nbsp;
                                <input type="radio" name="religion" value="Christian"/>Christian &nbsp;
                                <input type="radio" name="religion" value="Buddism"/>Buddhism &nbsp;
                                <input type="radio" name="religion" value="Others"/>Others &nbsp;
                            </td>

                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input readonly="true" type="email" required="" name="email" value="${Student.email}"/></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><input type="text" name="phone"/></td>
                        </tr>
                        <tr>
                            <td>Nationality</td>
                            <td><input type="text" name="nationality"/></td>
                        </tr>
                        <tr>
                            <td>Division</td>
                            <td>
                                <select name="division">
                                    <option>Select Division</option>
                                    <option value="Dhaka">Dhaka</option>
                                    <option value="Chittagong">Chittagong</option>
                                    <option value="Rajshahi">Rajshahi</option>
                                    <option value="Sylhet">Sylhet</option>
                                    <option value="Rongpur">Rangpur</option>
                                    <option value="Barishal">Barishal</option>
                                    <option value="Khuulna">Khulna</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>District</td>
                            <td><input type="text" name="district"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="hidden" name="image" value="${Student.image}"/></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Submit"/></td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>

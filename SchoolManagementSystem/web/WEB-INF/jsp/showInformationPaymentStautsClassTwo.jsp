<%-- 
    Document   : showInformationPaymentStautsClassTwo
    Created on : Oct 16, 2015, 10:19:14 AM
    Author     : Maksud Rahaman
--%>

<%@ include file="headerForAdmin.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSideBarForAdmin.jsp" %>
        </div>
        <div class="col-sm-6">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <table>
                <th>
                <tr>
                    <td>Roll Number</td>
                    <td>Student Name</td>
                    <td>Month Of Payment</td>
                    <td>Date Of Payment</td>
                    <td>Paying Amount</td>
                    <td>Advance Amount</td>
                    <td>Dues Amount</td>
                </tr>
                </th>
                <c:forEach items="${List}" var="list">
                    <tr>
                        <td>${list.classTwoAdmissionTable.rollNumberTwo}</td>
                        <td>${list.studentName}</td>
                        <td>${list.month}</td>
                        <td>${list.dateOfPayment}</td>
                        <td>${list.amount}</td>
                        <td>${list.advance}</td>
                        <td>${list.dues}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
<%@ include file="footerForAdmin.jsp" %>

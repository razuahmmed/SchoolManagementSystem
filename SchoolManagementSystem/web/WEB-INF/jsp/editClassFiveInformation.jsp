<%-- 
    Document   : editClassFiveInformation
    Created on : Oct 15, 2015, 1:47:31 PM
    Author     : Maksud Rahaman
--%>

<%@ include file="headerForAdmin.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSideBarForAdmin.jsp" %>
        </div>
        <div class="col-sm-6">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <form action="editInformationForClassFive.htm">
                <fieldset>
                    <legend>Edit Form For Class Five</legend>
                    <legend>Fill up the Form with Correct Information</legend>
                    <table class="table">
                        <tr>
                            <td>Roll Number</td>
                            <td><input type="text" name="rollNumberFive" value="${Student.rollNumberFive}"/></td>
                        </tr>
                        <tr>
                            <td>Serial Number</td>
                            <td><input type="text" name="serialNumberFiveadtest" 
                                       value="${Student.classFiveAdmissionTestTable.serialNumberFiveadtest}"/></td>
                        </tr>
                        <tr>
                            <td>Student's Name</td>
                            <td><input type="text" name="studentName" value="${Student.studentName}"/></td>
                            <td><img src="<c:url value="${Student.image}"></c:url>" style="height: 50px; width: 100px"/></td>
                        </tr>
                        <tr>
                            <td>Father's Name</td>
                            <td><input type="text" name="fatherName" value="${Student.fatherName}"/></td>
                        </tr>
                        <tr>
                            <td>Mother's Name</td>
                            <td><input type="text" name="motherName" value="${Student.motherName}"/></td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td><input type="date" name="dateOfBirth" value="${Student.dateOfBirth}"/></td>
                        </tr>
                        <tr>
                            <td>Present Address</td>
                            <td><input type="text" name="presentAddress" value="${Student.presentAddress}"/></td>
                        </tr>
                        <tr>
                            <td>Permanent Address</td>
                            <td><input type="text" name="permanentAddress" value="${Student.permanentAddress}"/></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>
                                <c:set var="gen" value="${Student.gender}"></c:set>
                                    <input type="radio" name="gender" value="Male" id="genMale"
                                    <c:if test="${gen=='Male'}">checked</c:if>/>Male &nbsp;
                                    <input type="radio" name="gender" value="Female" id="gemFemale"
                                    <c:if test="${gen=='Female'}">checked</c:if>/>Female &nbsp;
                                    <input type="radio" name="gender" value="Third"  id="gemThird"
                                    <c:if test="${gen=='Third'}">checked</c:if> />Third &nbsp;
                                </td>

                            </tr>
                            <tr>
                                <td>Religion</td>
                                <td>
                                <c:set var="rel" value="${Student.religion}"></c:set>
                                    <input type="radio" name="religion" value="Islam"
                                    <c:if test="${rel=='Islam'}">checked</c:if>/>Islam &nbsp;
                                    <input type="radio" name="religion" value="Hinduism"
                                    <c:if test="${rel=='Hinduism'}">checked</c:if>/>Hinduism &nbsp;
                                    <input type="radio" name="religion" value="Christian"
                                    <c:if test="${rel=='Christian'}">checked</c:if>/>Christian &nbsp;
                                    <input type="radio" name="religion" value="Buddism"
                                    <c:if test="${rel=='Buddism'}">checked</c:if>/>Buddhism &nbsp;
                                    <input type="radio" name="religion" value="Others"
                                    <c:if test="${rel=='Others'}">checked</c:if>/>Others &nbsp;

                                </td>

                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="hidden" name="admissionDate" value="${Student.admissionDate}"/></td>
                        
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><input type="text" name="email" value="${Student.email}"/></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><input type="text" name="phone" value="${Student.phone}"/></td>
                        </tr>
                        <tr>
                            <td>Nationality</td>
                            <td><input type="text" name="nationality" value="${Student.nationality}"/></td>
                        </tr>
                        <tr>
                            <td>Division</td>
                            <td>
                                <c:set var="divsn" value="${Student.division}"></c:set>
                                    <select name="division">
                                        <option>Select Division</option>
                                        <option value="Dhaka" <c:if test="${divsn=='Dhaka'}">selected</c:if>>Dhaka</option>
                                    <option value="Chittagong" <c:if test="${divsn=='Chittagong'}">selected</c:if>>Chittagong</option>
                                    <option value="Rajshahi" <c:if test="${divsn=='Rajshahi'}">selected</c:if>>Rajshahi</option>
                                    <option value="Sylhet" <c:if test="${divsn=='Sylhet'}">selected</c:if>>Sylhet</option>
                                    <option value="Rongpur" <c:if test="${divsn=='Rongpur'}">selected</c:if>>Rangpur</option>
                                    <option value="Barishal" <c:if test="${divsn=='Barishal'}">selected</c:if>>Barishal</option>
                                    <option value="Khuulna" <c:if test="${divsn=='Khuulna'}">selected</c:if>>Khulna</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>District</td>
                                <td><input type="text" name="district" value="${Student.district}"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="hidden" name="image" value="${Student.image}"/></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Submit"/></td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>

    </div>
</div>

<%@ include file="footerForAdmin.jsp" %>

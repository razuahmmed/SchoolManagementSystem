<%-- 
    Document   : adminSignupSuccess
    Created on : Oct 2, 2015, 3:48:52 AM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <h1>Signup Success</h1>
            <form action="adminLogin.htm">
                <fieldset>
                    <legend>Admin Login</legend>
                    <table>
                        <tr>
                            <td>Email</td>
                            <td>&nbsp;</td>
                            <td><input type="text" name="email"/></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td>&nbsp;</td>
                            <td><input type="password" name="password"/></td>
                        </tr>
                        <tr>
                            <td>Admin Code</td>
                            <td>&nbsp;</td>
                            <td><input type="password" name="admincode"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>&nbsp;</td>
                            <td><input type="submit" value="Submit"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>&nbsp;</td>
                            <td>${ErrorMessage}</td>
                        </tr>
                    </table>
                </fieldset>

            </form>
        </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>

<%@ include file="headerForAdmin.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSideBarForAdmin.jsp" %>
        </div>
        <div class="col-sm-6">
            <form action="createResultClassEight.htm">
                <fieldset>
                    <legend>Class Eight Result Create</legend>
                    <table class="table">
                        <tr>
                            <td>Roll Number</td>
                            <td><input type="text" name="rollNumberEight"/></td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td><input type="text" name="studentName"/></td>
                        </tr>
                        <tr>
                            <td style="color: red">Bangla</td>
                        </tr>
                        <tr>
                            <td>Full Marks</td>
                            <td><input type="text" name="banglaMarks" size="10" id="marksBangla"/></td>
                            <td>MCQ</td>
                            <td><input type="text" name="mcqBangla"  size="10" id="banglaMcq"/></td>
                            <td>Descriptive</td>
                            <td><input type="text" name="descBangla"  size="10" id="banglaDesc" onkeyup="banglaNumberCount()"/></td>
                            <td>Obtain Marks</td>
                            <td><input type="text" name="obtainBangla"  size="10" id="banglaObtain"/></td>
                            <td>Grade Point</td>
                            <td><input type="text" name="banglaGp"  size="10" id="banGp"/>
                                <p id="gpbangla"></p></td>
                        </tr>
                        <tr>
                            <td style="color: red">English</td>
                        </tr>
                        <tr>
                            <td>Full Marks</td>
                            <td><input type="text" name="englishMarks"  size="10" id="marksEnglish" /></td>
                            <td>Obtain Marks</td>
                            <td><input type="text" name="obtainEnglish"  size="10" id="englishObtain"
                                       onblur="englishNumberCount()"/></td>
                            <td>Grade Point</td>
                            <td><input type="text" name="englishGp"  size="10" id="gpEnglish"/></td>
                        </tr>
                        <tr>
                            <td style="color: red">Mathematics</td>
                        </tr>
                        <tr>
                            <td>Full Marks</td>
                            <td><input type="text" name="mathematicsMarks"  size="10" id="marksMath"/></td>
                            <td>Obtain Marks</td>
                            <td><input type="text" name="obtainMathemaitcs"  size="10" id="mathObtain" onblur="mathematicsNumberCount()"/></td>
                            <td>Grade Point</td>
                            <td><input type="text" name="mathematicsGp"  size="10" id="gpMath"/></td>
                        </tr>
                        <tr>
                            <td style="color: red">General Science</td>
                        </tr>
                        <tr>
                            <td>Full Marks</td>
                            <td><input type="text" name="gsMarks"  size="10" id="marksGS"/></td>
                            <td>MCQ</td>
                            <td><input type="text" name="mcqGs"  size="10" id="gsMcq"/></td>
                            <td>Descriptive</td>
                            <td><input type="text" name="descGs"  size="10" id="gsDesc" onkeyup="generalScienceNumberCount()"/></td>
                            <td>Obtain Marks</td>
                            <td><input type="text" name="obtainGs"  size="10" id="gsObtain"/></td>
                            <td>Grade Point</td>
                            <td><input type="text" name="gsGp"  size="10" id="gpGS"/></td>
                        </tr>
                        <tr>
                            <td style="color: red">Social Science</td>
                        </tr>
                        <tr>
                            <td>Full Marks:</td>
                            <td><input type="text" name="ssMarks"  size="10" id="marksSS"/></td>
                            <td>MCQ</td>
                            <td><input type="text" name="mcqSs"  size="10" id="ssMCQ"/></td>
                            <td>Descriptive:</td>
                            <td><input type="text" name="descSs"  size="10" id="ssDesc" onkeyup="socialScienceNumberCount()"/></td>
                            <td>Obtain Marks:</td>
                            <td><input type="text" name="obtainSs"  size="10" id="ssObtain"/></td>
                            <td>Grade Point:</td>
                            <td><input type="text" name="ssGp"  size="10" id="gpSS"/></td>
                        </tr>
                        <tr>
                            <td style="color: red">Religion</td>
                        </tr>
                        <tr>
                            <td>Full Marks:</td>
                            <td><input type="text" name="religionMarks"  size="10" id="marksReligion"/></td>
                            <td>MCQ</td>
                            <td><input type="text" name="mcqReligion"  size="10" id="religionMcq"/></td>
                            <td>Descriptive:</td>
                            <td><input type="text" name="descReligion"  size="10" id="religionDes" onkeyup="religionNumberCount()"/></td>
                            <td>Obtain Marks:</td>
                            <td><input type="text" name="obtainReligion"  size="10" id="religionObtain"/></td>
                            <td>Grade Point:</td>
                            <td><input type="text" name="religionGp"  size="10" id="gpReligion"/></td>
                        </tr>
                        <tr>
                            <td style="color: red">Computer</td>
                        </tr>
                        <tr>
                            <td>Full Marks:</td>
                            <td><input type="text" name="computerMarks"  size="10" id="marksComputer" onblur="totalMarksCount()"/></td>
                            <td>MCQ:</td>
                            <td><input type="text" name="mcqComputer"  size="10" id="computerMcq"/></td>
                            <td>Descriptive:</td>
                            <td><input type="text" name="descComputer"  size="10" id="computerDesc" 
                                       onblur="computerNumberCount()"/></td>
                            <td>Obtain Marks:</td>
                            <td><input type="text" name="obtainComputer"  size="10" id="computerObtain"/></td>
                            <td>Grade Point:</td>
                            <td><input type="text" name="computerGp"  size="10" id="gpComputer"/></td>
                        </tr>
                        <tr>
                            <td>Total Marks:</td>
                            <td><input type="text" name="grandTotalMarks"  size="10" id="totalGrandMarks"/></td>
                            <td>Obtain Total:</td>
                            <td><input type="text" name="obtainGrandTotal"  size="10" id="totalGrandObtain"/></td>
                        </tr>

                        <tr>
                            <td>GPA</td>
                            <td><input type="text" name="gpa"  size="10" id="totalGpa"/></td>
                            <td>Grade:</td>
                            <td><input type="text" name="grade"  size="10" id="totalGrade"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" value="Submit"  size="10"/></td>
                        </tr>
                    </table>
                </fieldset>
            </form>

        </div>

    </div>
</div>

<%@ include file="footerForAdmin.jsp" %>
<script>
    function banglaNumberCount() {

        var bnMcq = parseFloat(document.getElementById("banglaMcq").value);
        var bnDesc = parseFloat(document.getElementById("banglaDesc").value);
        var obtain = bnMcq + bnDesc;
        document.getElementById("banglaObtain").value = obtain;
        var obt = parseFloat(document.getElementById("banglaObtain").value);
        if (obt >= 80) {
            document.getElementById("banGp").value = 5;
        }
        else if (obt >= 70 && obt < 80) {
            document.getElementById("banGp").value = 4;
        }

        else if (obt >= 60 && obt < 70) {
            document.getElementById("banGp").value = 3.5;
        }
        else if (obt >= 50 && obt < 60) {
            document.getElementById("banGp").value = 3;
        }
        else if (obt >= 40 && obt < 50) {
            document.getElementById("banGp").value = 2;
        }
        else if (obt >= 33 && obt < 40) {
            document.getElementById("banGp").value = 1;
        }
        else {
            document.getElementById("banGp").value = 0;
        }

    }

    function englishNumberCount() {

        var obt = parseFloat(document.getElementById("englishObtain").value);
        if (obt >= 80) {
            document.getElementById("gpEnglish").value = 5;
        }
        else if (obt >= 70 && obt < 80) {
            document.getElementById("gpEnglish").value = 4;
        }

        else if (obt >= 60 && obt < 70) {
            document.getElementById("gpEnglish").value = 3.5;
        }
        else if (obt >= 50 && obt < 60) {
            document.getElementById("gpEnglish").value = 3;
        }
        else if (obt >= 40 && obt < 50) {
            document.getElementById("gpEnglish").value = 2;
        }
        else if (obt >= 33 && obt < 40) {
            document.getElementById("gpEnglish").value = 1;
        }
        else {
            document.getElementById("gpEnglish").value = 0;
        }

    }
    function mathematicsNumberCount() {

        var obt = parseFloat(document.getElementById("mathObtain").value);
        if (obt >= 80) {
            document.getElementById("gpMath").value = 5;
        }
        else if (obt >= 70 && obt < 80) {
            document.getElementById("gpMath").value = 4;
        }

        else if (obt >= 60 && obt < 70) {
            document.getElementById("gpMath").value = 3.5;
        }
        else if (obt >= 50 && obt < 60) {
            document.getElementById("gpMath").value = 3;
        }
        else if (obt >= 40 && obt < 50) {
            document.getElementById("gpMath").value = 2;
        }
        else if (obt >= 33 && obt < 40) {
            document.getElementById("gpMath").value = 1;
        }
        else {
            document.getElementById("gpMath").value = 0;
        }

    }

    function computerNumberCount() {

        var bnMcq = parseFloat(document.getElementById("computerMcq").value);
        var bnDesc = parseFloat(document.getElementById("computerDesc").value);
        var obtain = bnMcq + bnDesc;
        document.getElementById("computerObtain").value = obtain;
        if (obtain >= 80) {
            document.getElementById("gpComputer").value = 5;
        }
        else if (obtain >= 70 && obtain < 80) {
            document.getElementById("gpComputer").value = 4;
        }

        else if (obtain >= 60 && obtain < 70) {
            document.getElementById("gpComputer").value = 3.5;
        }
        else if (obtain >= 50 && obtain < 60) {
            document.getElementById("gpComputer").value = 3;
        }
        else if (obtain >= 40 && obtain < 50) {
            document.getElementById("gpComputer").value = 2;
        }
        else if (obtain >= 33 && obtain < 40) {
            document.getElementById("gpComputer").value = 1;
        }
        else {
            document.getElementById("gpComputer").value = 0;
        }

        var bnObtain = parseFloat(document.getElementById("banglaObtain").value);
        var enObtain = parseFloat(document.getElementById("englishObtain").value);
        var mathObtain = parseFloat(document.getElementById("mathObtain").value);
        var gsObtain = parseFloat(document.getElementById("gsObtain").value);
        var ssObtain = parseFloat(document.getElementById("ssObtain").value);
        var relObtain = parseFloat(document.getElementById("religionObtain").value);
        var comObtain = parseFloat(document.getElementById("computerObtain").value);
        var totalObtain = bnObtain + enObtain + mathObtain + gsObtain + ssObtain + relObtain + comObtain;
        document.getElementById("totalGrandObtain").value = totalObtain;

        var bnGrade = parseFloat(document.getElementById("banGp").value);
        var enGrade = parseFloat(document.getElementById("gpEnglish").value);
        var mathGrade = parseFloat(document.getElementById("gpMath").value);
        var gsGrade = parseFloat(document.getElementById("gpGS").value);
        var ssGrade = parseFloat(document.getElementById("gpSS").value);
        var relGrade = parseFloat(document.getElementById("gpReligion").value);
        var comGrade = parseFloat(document.getElementById("gpComputer").value);
        var grade = parseFloat(bnGrade + enGrade + mathGrade + gsGrade + ssGrade + relGrade + comGrade) / 7;
        document.getElementById("totalGpa").value = grade;

        var totalGp = parseFloat(document.getElementById("totalGpa").value);
        if (totalGp >= 5) {
            document.getElementById("totalGrade").value = "A+";
        }
        else if (totalGp >= 4 && totalGp < 5) {
            document.getElementById("totalGrade").value = "A";
        }

        else if (totalGp >= 3.5 && totalGp < 4) {
            document.getElementById("totalGrade").value = "A-";
        }
        else if (totalGp >= 3 && totalGp < 3.5) {
            document.getElementById("totalGrade").value = "B";
        }
        else if (totalGp >= 2 && totalGp < 3) {
            document.getElementById("totalGrade").value = "C";
        }
        else if (totalGp >= 1 && totalGp < 2) {
            document.getElementById("totalGrade").value = "D";
        }
        else {
            document.getElementById("totalGrade").value = "F";
        }
    }


    function socialScienceNumberCount() {

        var bnMcq = parseFloat(document.getElementById("ssMCQ").value);
        var bnDesc = parseFloat(document.getElementById("ssDesc").value);
        var obtain = bnMcq + bnDesc;
        document.getElementById("ssObtain").value = obtain;
        var obt = parseInt(document.getElementById("ssObtain").value);
        if (obt >= 80) {
            document.getElementById("gpSS").value = 5;
        }
        else if (obt >= 70 && obt < 80) {
            document.getElementById("gpSS").value = 4;
        }

        else if (obt >= 60 && obt < 70) {
            document.getElementById("gpSS").value = 3.5;
        }
        else if (obt >= 50 && obt < 60) {
            document.getElementById("gpSS").value = 3;
        }
        else if (obt >= 40 && obt < 50) {
            document.getElementById("gpSS").value = 2;
        }
        else if (obt >= 33 && obt < 40) {
            document.getElementById("gpSS").value = 1;
        }
        else {
            document.getElementById("gpSS").value = 0;
        }

    }

    function generalScienceNumberCount() {

        var bnMcq = parseFloat(document.getElementById("gsMcq").value);
        var bnDesc = parseFloat(document.getElementById("gsDesc").value);
        var obtain = bnMcq + bnDesc;
        document.getElementById("gsObtain").value = obtain;
        var obt = parseFloat(document.getElementById("gsObtain").value);
        if (obt >= 80) {
            document.getElementById("gpGS").value = 5;
        }
        else if (obt >= 70 && obt < 80) {
            document.getElementById("gpGS").value = 4;
        }

        else if (obt >= 60 && obt < 70) {
            document.getElementById("gpGS").value = 3.5;
        }
        else if (obt >= 50 && obt < 60) {
            document.getElementById("gpGS").value = 3;
        }
        else if (obt >= 40 && obt < 50) {
            document.getElementById("gpGS").value = 2;
        }
        else if (obt >= 33 && obt < 40) {
            document.getElementById("gpGS").value = 1;
        }
        else {
            document.getElementById("gpGS").value = 0;
        }

    }

    function totalMarksCount() {
        var bnFull = parseFloat(document.getElementById("marksBangla").value);
        var enFull = parseFloat(document.getElementById("marksEnglish").value);
        var mathFull = parseFloat(document.getElementById("marksMath").value);
        var gsFull = parseFloat(document.getElementById("marksGS").value);
        var ssFull = parseFloat(document.getElementById("marksSS").value);
        var religionFull = parseFloat(document.getElementById("marksReligion").value);
        var computerFull = parseFloat(document.getElementById("marksComputer").value);
        var obtain = bnFull + enFull + mathFull + gsFull + ssFull + religionFull + computerFull;
        document.getElementById("totalGrandMarks").value = obtain;
    }

    function religionNumberCount() {

        var bnMcq = parseFloat(document.getElementById("religionMcq").value);
        var bnDesc = parseFloat(document.getElementById("religionDes").value);
        var obtain = bnMcq + bnDesc;
        document.getElementById("religionObtain").value = obtain;
        var obt = parseFloat(document.getElementById("religionObtain").value);
        if (obt >= 80) {
            document.getElementById("gpReligion").value = 5;
        }
        else if (obt >= 70 && obt < 80) {
            document.getElementById("gpReligion").value = 4;
        }

        else if (obt >= 60 && obt < 70) {
            document.getElementById("gpReligion").value = 3.5;
        }
        else if (obt >= 50 && obt < 60) {
            document.getElementById("gpReligion").value = 3;
        }
        else if (obt >= 40 && obt < 50) {
            document.getElementById("gpReligion").value = 2;
        }
        else if (obt >= 33 && obt < 40) {
            document.getElementById("gpReligion").value = 1;
        }
        else {
            document.getElementById("gpReligion").value = 0;
        }

    }

    function countTotalGrade() {
//    var totalGp = parseFloat(document.getElementById("totalGpa").value);
//            if (totalGp >= 5) {
//    document.getElementById("totalGrade").value = "A+";
//    }
//    else if (totalGp >= 4 && totalGp < 5) {
//    document.getElementById("totalGrade").value = "A";
//    }
//
//    else if (totalGp >= 3.5 && totalGp < 4) {
//    document.getElementById("totalGrade").value = "A-";
//    }
//    else if (totalGp >= 3 && totalGp < 3.5) {
//    document.getElementById("totalGrade").value = "B";
//    }
//    else if (totalGp >= 2 && totalGp < 3) {
//    document.getElementById("totalGrade").value = "C";
//    }
//    else if (totalGp >= 1 && totalGp < 2) {
//    document.getElementById("totalGrade").value = "D";
//    }
//    else {
//    document.getElementById("totalGrade").value = "F";
//    }
    }




</script>

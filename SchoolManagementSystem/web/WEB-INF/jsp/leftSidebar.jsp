<%-- 
    Document   : leftSidebar
    Created on : Sep 10, 2015, 2:05:56 PM
    Author     : J2EE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <nav>
            <div class="menu-item alpha">
                <h4><a href="#">Apply Online &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <li><a href="goToClassOneTest.htm">Class One</a></li>
                    <li><a href="goToClassTwoTest.htm">Class Two</a></li>
                    <li><a href="goToClassThreeTest.htm">Class Three</a></li>
                    <li><a href="goToClassFourTest.htm">Class Four</a></li>
                    <li><a href="goToClassFiveTest.htm">Class Five</a></li>
                    <li><a href="goToClassSixTest.htm">Class Six</a></li>
                    <li><a href="goToClassSevenTest.htm">Class Seven</a></li>
                    <li><a href="goToClassEightTest.htm">Class Eight</a></li>
                    <li><a href="goToClassNineTest.htm">Class Nine</a></li>


                </ul>
            </div>

            <div class="menu-item alpha">
                <h4><a href="#">Download Admission Test Admit Card &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <li><a href="goToDownloadAdmitCardOne.htm">Class One</a></li>
                    <li><a href="goToDownloadAdmitCardTwo.htm">Class Two</a></li>
                    <li><a href="goToDownloadAdmitCardThree.htm">Class Three</a></li>
                    <li><a href="goToDownloadAdmitCardFour.htm">Class Four</a></li>
                    <li><a href="goToDownloadAdmitCardFive.htm">Class Five</a></li>
                    <li><a href="goToDownloadAdmitCardSix.htm">Class Six</a></li>
                    <li><a href="goToDownloadAdmitCardSeven.htm">Class Seven</a></li>
                    <li><a href="goToDownloadAdmitCardEight.htm">Class Eight</a></li>
                    <li><a href="goToDownloadAdmitCardNine.htm">Class Nine</a></li>
                    
                   
                </ul>

            </div>

            <div class="menu-item alpha">
                <h4><a href="#">Apply For Admission  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                     <li><a href="goToConfirmAdmissionClassOne.htm">Class One</a></li>
                    <li><a href="goToConfirmAdmissionClassTwo.htm">Class Two</a></li>
                    <li><a href="goToConfirmAdmissionClassThree.htm">Class Three</a></li>
                    <li><a href="goToConfirmAdmissionClassFour.htm">Class Four</a></li>
                    <li><a href="goToConfirmAdmissionClassFive.htm">Class Five</a></li>
                    <li><a href="goToConfirmAdmissionClassSix.htm">Class Six</a></li>
                    <li><a href="goToConfirmAdmissionClassSeven.htm">Class Seven</a></li>
                    <li><a href="goToConfirmAdmissionClassEight.htm">Class Eight</a></li>
                    <li><a href="goToConfirmAdmissionClassNine.htm">Class Nine</a></li>
                    
                    

                </ul>

            </div>

            <div class="menu-item alpha ">
                <h4><a href="#">Notice Board  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <li><a href="goToDownloadTeacherNotice.htm">Teachers' Notices</a></li>
                    <li><a href="goToDownloadStudentNotice.htm">Students' Notices</a></li>
<!--                    <li><a href="goToCreateResultClassThree.htm">Class Three</a></li>
                    <li><a href="goToCreateResultClassFour.htm">Class Four</a></li>
                    <li><a href="goToCreateResultClassFive.htm">Class Five</a></li>
                    <li><a href="goToCreateResultClassSix.htm">Class Six</a></li>
                    <li><a href="goToCreateResultClassSeven.htm">Class Seven</a></li>
                    <li><a href="goToCreateResultClassEight.htm">Class Eight</a></li>
                    <li><a href="goToCreateResultClassNine.htm">Class Nine</a></li>
                    <li><a href="goToCreateResultClassTen.htm">Class Ten</a></li>-->

                </ul>

            </div>
            <div class="menu-item alpha ">
                <h4><a href="#">Exam Results  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <ul>
                        <li><a href="goToSearchResultClassOne.htm">Class One</a></li>
                        <li><a href="goToSearchResultClassTwo.htm">Class Two</a></li>
                        <li><a href="goToSearchResultClassThree.htm">Class Three</a></li>
                        <li><a href="goToSearchResultClassFour.htm">Class Four</a></li>
                        <li><a href="goToSearchResultClassFive.htm">Class Five</a></li>
                        <li><a href="goToSearchResultClassSix.htm">Class Six</a></li>
                        <li><a href="goToSearchResultClassSeven.htm">Class Seven</a></li>
                        <li><a href="goToSearchResultClassEight.htm">Class Eight</a></li>
                        <li><a href="goToSearchResultClassNine.htm">Class Nine</a></li>
                        <li><a href="goToSearchResultClassTen.htm">Class Ten</a></li>

                    </ul>

                </ul>

            </div>
            <div class="menu-item alpha ">
                <h4><a href="#">Students' Daily Attendance Status   &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <li><a href="goToShowAttendanceForClassOne.htm">Class One</a></li>
                    <li><a href="goToShowAttendanceForClassTwo.htm">Class Two</a></li>
                    <li><a href="goToShowAttendanceForClassThree.htm">Class Three</a></li>
                    <li><a href="goToShowAttendanceForClassFour.htm">Class Four</a></li>
                    <li><a href="goToShowAttendanceForClassFive.htm">Class Five</a></li>
                    <li><a href="goToShowAttendanceForClassSix.htm">Class Six</a></li>
                    <li><a href="goToShowAttendanceForClassSeven.htm">Class Seven</a></li>
                    <li><a href="goToShowAttendanceForClassEight.htm">Class Eight</a></li>
                    <li><a href="goToShowAttendanceForClassNine.htm">Class Nine</a></li>
                    <li><a href="goToShowAttendanceForClassTen.htm">Class Ten</a></li>
                    
                   
                </ul>
            </div>

            <div class="menu-item alpha">
                <h4><a href="#">Teachers' Information &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <li><a href="showAllTeacherInformation.htm">Available Teachers</a></li>
                </ul>

            </div>

            <div class="menu-item alpha">
                <h4><a href="#">No Caption  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
<!--                    <li><a href="goToSearchStatusClassOne.htm">Class One</a></li>
                    <li><a href="goToSearchStatusClassTwo.htm">Class Two</a></li>
                    <li><a href="goToSearchStatusClassThree.htm">Class Three</a></li>
                    <li><a href="goToSearchStatusClassFour.htm">Class Four</a></li>
                    <li><a href="goToSearchStatusClassFive.htm">Class Five</a></li>
                    <li><a href="goToSearchStatusClassSix.htm">Class Six</a></li>
                    <li><a href="goToSearchStatusClassSeven.htm">Class Seven</a></li>
                    <li><a href="goToSearchStatusClassEight.htm">Class Eight</a></li>
                    <li><a href="goToSearchStatusClassNine.htm">Class Nine</a></li>
                    <li><a href="goToSearchStatusClassTen.htm">Class Ten</a></li>-->

                </ul>

            </div>

            <div class="menu-item alpha ">
                <h4><a href="#">No Caption  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
<!--                <ul>
                    <li><a href="goToSearchFeesPaymentStatusClassOne.htm">Class One</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassTwo.htm">Class Two</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassThree.htm">Class Three</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassFour.htm">Class Four</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassFive.htm">Class Five</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassSix.htm">Class Six</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassSeven.htm">Class Seven</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassEight.htm">Class Eight</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassNine.htm">Class Nine</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassTen.htm">Class Ten</a></li>

                </ul>-->

            </div>
            <div class="menu-item alpha ">
                <h4><a href="#">No Caption  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
<!--                     <li><a href="goToShowInfoToPayClassOneFees.htm">Class One</a></li>
                    <li><a href="goToShowInfoToPayClassTwoFees.htm">Class Two</a></li>
                    <li><a href="goToShowInfoToPayClassThreeFees.htm">Class Three</a></li>
                    <li><a href="goToShowInfoToPayClassFourFees.htm">Class Four</a></li>
                    <li><a href="goToShowInfoToPayClassFiveFees.htm">Class Five</a></li>
                    <li><a href="goToShowInfoToPayClassSixFees.htm">Class Six</a></li>
                    <li><a href="goToShowInfoToPayClassSevenFees.htm">Class Seven</a></li>
                    <li><a href="goToShowInfoToPayClassEightFees.htm">Class Eight</a></li>
                    <li><a href="goToShowInfoToPayClassNineFees.htm">Class Nine</a></li>
                    <li><a href="goToShowInfoToPayClassTenFees.htm">Class Ten</a></li>-->

                </ul>

            </div>
        </nav>
    </body>
</html>

<%-- 
    Document   : studentFeesPaymentClassSix
    Created on : Oct 14, 2015, 10:05:58 AM
    Author     : J2EE 23
--%>

<%@ include file="headerForAdmin.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSideBarForAdmin.jsp" %>
        </div>
        <div class="col-sm-6">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <form action="classSixPayment.htm">
                <fieldset>
                    <legend>Class Six Payment</legend>
                    <table>
                        <tr>
                            <td>Roll Number</td>
                            <td><input type="text" name="rollNumberSix" value="${Roll}"/></td>
                        </tr>
                        <tr>
                            <td>Student Name</td>
                            <td><input type="text" name="studentName" value="${Student.studentName}"/></td>
                        </tr>
                        <tr>
                            <td>Month</td>
                            <td>
                                <select name="month">
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td>Amount</td>
                            <td><input type="text" name="amount"/></td>
                        </tr>
                        <tr>
                            <td>Advance</td>
                            <td><input type="text" name="advance" value="${List.advance}"/></td>
                        </tr>
                        <tr>
                            <td>Dues</td>
                            <td><input type="text" name="dues" value="${List.dues}"/></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Submit"/></td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>

    </div>
</div>

<%@ include file="footerForAdmin.jsp" %>

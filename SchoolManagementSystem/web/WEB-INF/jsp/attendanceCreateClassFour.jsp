<%-- 
    Document   : attendanceCreateClassFour
    Created on : Oct 15, 2015, 6:59:39 AM
    Author     : Maksud Rahaman
--%>

<%@ include file="headerForAdmin.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSideBarForAdmin.jsp" %>
        </div>
        <div class="col-sm-6">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <form action="classFourAttendance.htm">
                <fieldset>
                    <legend>Class Six Attendance</legend>
                    <table>
                        <tr>
                            <td>Roll Number</td>
                            <td><input type="text" name="rollNumberFour" value="${Roll}"/></td>
                        </tr>
                        <tr>
                            <td>Student Name</td>
                            <td><input type="text" name="studentName" value="${Student.studentName}"/></td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>
                                <select name="status">

                                    <option value="">Select Status</option>
                                    <option value="Absent">Absent</option>
                                    <option value="Present">Present</option>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td>Count Absent</td>
                            <td><input type="number" name="countAbsent" value="${List.classTeacher}"/></td>
                        </tr>
                        <tr>
                            <td>Count Present</td>
                            <td><input type="number" name="countPresent" value="${List.countPresent}"/></td>
                        </tr>
                        <tr>
                            <td>Comment Of Teacher</td>
                            <td><input type="text" name="classTeacher" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>${Message}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" value="Submit"/></td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>

    </div>
</div>

<%@ include file="footerForAdmin.jsp" %>

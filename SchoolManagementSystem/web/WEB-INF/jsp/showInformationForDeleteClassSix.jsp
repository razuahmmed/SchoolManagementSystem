<%-- 
    Document   : showInformationForPayingPayment
    Created on : Sep 28, 2015, 5:27:44 PM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <fieldset>
                <legend> Class Six Students' Information</legend>
                <table>
                    <th>
                    <tr>
                        <td>Roll </td>
                        <td> Name</td>
                        <td>Guardian</td>
                        <td>Birth Date</td>
                        <td>Religion</td>
                        <td>Email</td>
                        <td>Phone</td>
                        <td>Nationality</td>
                    </tr>
                    </th>

                    <c:forEach items="${List}" var="list">
                        <tr>
                            <td>${list.rollNumberSix}</td>
                            <td>${list.studentName}</td>
                            <td>${list.fatherName}</td>
                            <td>${list.dateOfBirth}</td>
                            <td>${list.religion}</td>
                            <td>${list.email}</td>
                            <td>${list.phone}</td>
                            <td>${list.nationality}</td> 
                            <td><a href="goToPayClassSixFees?rollNumberSix=${list.rollNumberSix}&studentName=${list.studentName}">Pay Fees</a></td> 
                        </tr>
                    </c:forEach>
                </table>
            </fieldset>

        </div>

    </div>
</div>

<%@ include file="footer.jsp" %>

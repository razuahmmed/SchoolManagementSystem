<%-- 
    Document   : searchPaymentStatusNine
    Created on : Oct 16, 2015, 9:51:47 AM
    Author     : Maksud Rahaman
--%>

<%@ include file="headerForAdmin.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSideBarForAdmin.jsp" %>
        </div>
        <div class="col-sm-6">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <form action="searchPaymentStatusClassNine.htm">
                <fieldset>
                    <legend>
                        Class Nine Payment Status
                    </legend>
                    <table>
                        <th>
                        <tr>
                            <td>Roll Number</td>

                        </tr> 
                        </th>
                        <tr>
                            <td><input type="int" name="rollNumberNine"></td>

                        </tr> 
                        <tr>

                            <td><input type="submit"  value="Submit"></td>

                        </tr> 

                    </table>
                </fieldset>


            </form>
        </div>
    </div>
</div>
<%@ include file="footerForAdmin.jsp" %>

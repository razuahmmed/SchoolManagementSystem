<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <form action="insertClassEightAdmissionTest.htm">
                <fieldset style="width: 100%;">
                    <legend>Class Eight Admission Test Form</legend>
                    <table>

                        <tr>
                            <td>Student's Name</td>
                            <td><input type="text" required="true" placeholder="Enter Name" name="studentName"/></td>
                        </tr>
                        <tr>
                            <td>Father's Name</td>
                            <td><input type="text" name="fatherName" required="true" placeholder="Father's Name"/></td>
                        </tr>
                        <tr>
                            <td>Mother's Name</td>
                            <td><input type="text" name="motherName" required="true" placeholder="Mother's Name"/></td>
                        </tr>
                        <tr>
                            <td>Date Of Birth</td>
                            <td><input type="date" name="dateOfBirht" placeholder="yyyy-MM-dd" required="true" /> &nbsp;
                                From 2003-01-01 to 2005-01-01 </td>
                        </tr>
                        <!--                        <tr>
                                                    <td>Apply Date</td>
                                                    <td><input type="text" name="applyDate" placeholder="yyyy-MM-dd"/>
                        ${errorDobMessage}
                    </td>

                </tr>-->
                        <tr>
                            <td>Present Address</td>
                            <td><input type="text" name="presentAddress" required="true" placeholder="Present Address"/></td>
                        </tr>
                        <tr>
                            <td>Permanent Address</td>
                            <td><input type="text" name="permanentAddress" required="true" placeholder="Permanent Address"/></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>
                                <input type="radio" name="gender" value="Male"/>Male &nbsp;
                                <input type="radio" name="gender" value="Female"/>Female &nbsp;
                                <input type="radio" name="gender" value="Third"/>Third &nbsp;
                            </td>

                        </tr>
                        <tr>
                            <td>Religion</td>
                            <td>
                                <input type="radio" name="religion" value="Islam"/>Islam &nbsp;
                                <input type="radio" name="religion" value="Hinduism"/>Hinduism &nbsp;
                                <input type="radio" name="religion" value="Christian"/>Christian &nbsp;
                                <input type="radio" name="religion" value="Buddism"/>Buddhism &nbsp;
                                <input type="radio" name="religion" value="Others"/>Others &nbsp;
                            </td>

                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input type="email" name="email" required="true" placeholder="Email"/></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><input type="text" name="phone" required="true" placeholder="Phone"/></td>
                        </tr>
                        <tr>
                            <td>Nationality</td>
                            <td><input type="text" name="nationlity" required="true" placeholder="Nationality"/></td>
                        </tr>
                        <tr>
                            <td>Division</td>
                            <td>
                                <select name="division">
                                    <option>Select Division</option>
                                    <option value="Dhaka">Dhaka</option>
                                    <option value="Chittagong">Chittagong</option>
                                    <option value="Rajshahi">Rajshahi</option>
                                    <option value="Sylhet">Sylhet</option>
                                    <option value="Rongpur">Rongpur</option>
                                    <option value="Barishal">Barishal</option>
                                    <option value="Khuulna">Khuulna</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>District</td>
                            <td><input type="text" name="district" required="true" placeholder="District"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="hidden" name="image"/></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Submit"/></td>
                        </tr>
                    </table>
                </fieldset>

            </form> 
           
        </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>


<%-- 
    Document   : welcomeAdmissionForClassSix
    Created on : Oct 2, 2015, 1:22:38 AM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6" id="printableArea">
            <fieldset>
                <legend>Welcome To ----------- School and College.</legend>
                <legend>You are admitted into Class Six. Here is Your Information.</legend>
                <table>
                    tr>
                        <td>Class:</td>
                        <td>Six</td>
                        <td>Date:</td>
                        <td><input type="date" value="${CurrentDate}" readonly="true"></td>
                    </tr>
                    <tr>
                        <td>Student's Name:</td>
                        <td>${Student.studentName}</td>
                         <td></td>
                        <td><img style="height: 50px; width: 100px" src="<c:url value="${Student.image}"></c:url>"></td>
                    </tr>
                    <tr>
                        <td>Class Roll:</td>
                        
                        <td>${Student.rollNumberSix}</td>
                    </tr>
                    <tr>
                        <td>Father's Name:</td>
                        
                        <td>${Student.fatherName}</td>
                    </tr>
                    <tr>
                        <td>Mother's Name:</td>
                        
                        <td>${Student.motherName}</td>
                    </tr>
                    <tr>
                        <td>Date of Birth:</td>
                        <td><input type="date" value="${Student.dateOfBirth}" readonly="true"></td>
                    </tr>
                    <tr>
                        <td>Present Address:</td>
                        
                        <td>${Student.presentAddress}</td>
                    </tr>
                    <tr>
                        <td>Permanent Address:</td>
                        
                        <td>${Student.permanentAddress}</td>
                    </tr>
                    <tr>
                        <td>Sex:</td>
                        
                        <td>${Student.gender}</td>
                    </tr>
                    <tr>
                        <td>Religion:</td>
                        
                        <td>${Student.religion}</td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        
                        <td>${Student.email}</td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        
                        <td>${Student.phone}</td>
                    </tr>
                    <tr>
                        <td>Nationality:</td>
                        
                        <td>${Student.nationality}</td>
                    </tr>
                    <tr>
                        <td>Division:</td>
                        
                        <td>${Student.division}</td>
                    </tr>
                    <tr>
                        <td>District:</td>
                        
                        <td>${Student.district}</td>
                    </tr>
                </table>
            </fieldset>

        </div>
                     <input type="button" onclick="printDiv('printableArea')" value="Print!" />
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById("printableArea").innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>


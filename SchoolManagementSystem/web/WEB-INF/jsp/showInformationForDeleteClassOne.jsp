<%-- 
    Document   : showInformationForDeleteClassOne
    Created on : Oct 2, 2015, 8:48:03 AM
    Author     : Maksud Rahaman
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="deleteInformationClassOne.htm">
            <fieldset>
                <legend>Delete Form For Class One</legend>
                <legend>Fill up the Form with Correct Information</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name="rollNumberOne" value="${Student.rollNumberOne}"/></td>
                    </tr>
                    <tr>
                        <td>Serial Number</td>
                        <td><input type="text" name="serialNumberOneadtest" value="${Student.classOneAdmissionTestTable.serialNumberOneadtest}"/></td>
                    </tr>
                    <tr>
                        <td>Student's Name</td>
                        <td><input type="text" name="studentName" value="${Student.studentName}"/></td>
                    </tr>
                    <tr>
                        <td>Father's Name</td>
                        <td><input type="text" name="fatherName" value="${Student.fatherName}"/></td>
                    </tr>
                    <tr>
                        <td>Mother's Name</td>
                        <td><input type="text" name="motherName" value="${Student.motherName}"/></td>
                    </tr>
                    <tr>
                        <td>Date of Birth</td>
                        <td><input type="date" name="dateOfBirth" value="${Student.dateOfBirth}"/></td>
                    </tr>
                    <tr>
                        <td>Present Address</td>
                        <td><input type="text" name="presentAddress" value="${Student.presentAddress}"/></td>
                    </tr>
                    <tr>
                        <td>Permanent Address</td>
                        <td><input type="text" name="permanentAddress" value="${Student.permanentAddress}"/></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td>
                            <input type="radio" name="gender" value="Male" value="${Student.gender}"/>Male &nbsp;
                            <input type="radio" name="gender" value="Female" value="${Student.gender}"/>Female &nbsp;
                            <input type="radio" name="gender" value="Third" value="${Student.gender}"/>Third &nbsp;
                        </td>

                    </tr>
                    <tr>
                        <td>Religion</td>
                        <td>
                            <input type="radio" name="religion" value="Islam" value="${Student.religion}"/>Islam &nbsp;
                            <input type="radio" name="religion" value="Hinduism" value="${Student.religion}"/>Hinduism &nbsp;
                            <input type="radio" name="religion" value="Christian" value="${Student.religion}"/>Christian &nbsp;
                            <input type="radio" name="religion" value="Buddism" value="${Student.religion}"/>Buddhism &nbsp;
                            <input type="radio" name="religion" value="Others" value="${Student.religion}"/>Others &nbsp;
                            
                        </td>

                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="hidden" name="admissionDate" value="${Student.admissionDate}"/></td>
                        <img src="<c:url value="${Student.image}"></c:url>" width="50px" height="50px"/>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><input type="text" name="email" value="${Student.email}"/></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><input type="text" name="phone" value="${Student.phone}"/></td>
                    </tr>
                    <tr>
                        <td>Nationality</td>
                        <td><input type="text" name="nationality" value="${Student.nationality}"/></td>
                    </tr>
                    <tr>
                        <td>Division</td>
                        <td>
                            <select name="division" value="${Student.division}">
                                <option>Select Division</option>
                                <option value="Dhaka">Dhaka</option>
                                <option value="Chittagong">Chittagong</option>
                                <option value="Rajshahi">Rajshahi</option>
                                <option value="Sylhet">Sylhet</option>
                                <option value="Rongpur">Rangpur</option>
                                <option value="Barishal">Barishal</option>
                                <option value="Khuulna">Khulna</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>District</td>
                        <td><input type="text" name="district" value="${Student.district}"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="hidden" name="image" value="${Student.image}"/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Delete"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </body>
</html>

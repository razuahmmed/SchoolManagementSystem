<%@ include file="headerForAdmin.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSideBarForAdmin.jsp" %>
        </div>
        <div class="col-sm-6">
        <form action="createResultClassOne.htm">
            <fieldset>
                <legend>Class One Result Create</legend>
                <table class="table">
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name="rollNumberOne"/></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td><input type="text" name="studentName"/></td>
                    </tr>
                    <tr>
                        <td>Bangla</td>
                    </tr>
                    <tr>
                        <td>Full Marks</td>
                        <td><input type="text" name="banglaMarks" id="marksBangla"/></td>
                        <td>Obtain Marks</td>
                        <td><input type="text" name="obtainBangla" id="banglaObtain" onblur="banglaNumberCount()"/></td>
                        <td>Grade Point</td>
                        <td><input type="text" name="banglaGp" id="gpbangla"/></td>
                    </tr>
                    <tr>
                        <td>English</td>
                    </tr>
                    <tr>
                        <td>Full Marks</td>
                        <td><input type="text" name="englishMarks" id="marksEnglish"/></td>
                        <td>Obtain Marks</td>
                        <td><input type="text" name="obtainEnglish" id="englishObtain" onblur="englishNumberCount()"/></td>
                        <td>Grade Point</td>
                        <td><input type="text" name="englishGp" id="gpEnglish"/></td>
                    </tr>
                    <tr>
                        <td>Mathematics</td>
                    </tr>
                    <tr>
                        <td>Full Marks</td>
                        <td><input type="text" name="mathematicsMarks" id="marksMath" onblur="totalMarksCount()"/></td>
                        <td>Obtain Marks</td>
                        <td><input type="text" name="obtainMathematics" id="mathObtain" onblur="mathematicsNumberCount()"/></td>
                        <td>Grade Point</td>
                        <td><input type="text" name="mathematicsGp" id="gpMath"/></td>
                    </tr>
                    
                        <td>Total Marks</td>
                        <td><input type="text" name="grandTotalMarks" id="totalGrandMarks"/></td>
                    </tr>
                    <tr>
                        <td>Obtain Grand Total</td>
                        <td><input type="text" name="obtainGrandTotal" id="totalGrandObtain"/></td>
                    </tr>
                    <tr>
                        <td>GPA</td>
                        <td><input type="text" name="gpa" id="totalGpa"/></td>
                    </tr>
                    <tr>
                        <td>Grade</td>
                        <td><input type="text" name="grade" id="totalGrade"/></td>
                    </tr>
                    <tr>
                        
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
       
    </div>
</div>

<%@ include file="footerForAdmin.jsp" %>
<script>
    function totalMarksCount() {
        var bnFull = parseFloat(document.getElementById("marksBangla").value);
        var enFull = parseFloat(document.getElementById("marksEnglish").value);
        var mathFull = parseFloat(document.getElementById("marksMath").value);
        var obtain = bnFull + enFull + mathFull;
        document.getElementById("totalGrandMarks").value = obtain;
    }
    
    function banglaNumberCount() {

        var obt = parseFloat(document.getElementById("banglaObtain").value);
        if (obt >= 80) {
            document.getElementById("gpbangla").value = 5;
        }
        else if (obt >= 70 && obt < 80) {
            document.getElementById("gpbangla").value = 4;
        }

        else if (obt >= 60 && obt < 70) {
            document.getElementById("gpbangla").value = 3.5;
        }
        else if (obt >= 50 && obt < 60) {
            document.getElementById("gpbangla").value = 3;
        }
        else if (obt >= 40 && obt < 50) {
            document.getElementById("gpbangla").value = 2;
        }
        else if (obt >= 33 && obt < 40) {
            document.getElementById("gpbangla").value = 1;
        }
        else {
            document.getElementById("gpbangla").value = 0;
        }

    }
    
    function englishNumberCount() {

        var obt = parseFloat(document.getElementById("englishObtain").value);
        if (obt >= 80) {
            document.getElementById("gpEnglish").value = 5;
        }
        else if (obt >= 70 && obt < 80) {
            document.getElementById("gpEnglish").value = 4;
        }

        else if (obt >= 60 && obt < 70) {
            document.getElementById("gpEnglish").value = 3.5;
        }
        else if (obt >= 50 && obt < 60) {
            document.getElementById("gpEnglish").value = 3;
        }
        else if (obt >= 40 && obt < 50) {
            document.getElementById("gpEnglish").value = 2;
        }
        else if (obt >= 33 && obt < 40) {
            document.getElementById("gpEnglish").value = 1;
        }
        else {
            document.getElementById("gpEnglish").value = 0;
        }

    }
    
    function mathematicsNumberCount() {

        var obt = parseFloat(document.getElementById("mathObtain").value);
        if (obt >= 80) {
            document.getElementById("gpMath").value = 5;
        }
        else if (obt >= 70 && obt < 80) {
            document.getElementById("gpMath").value = 4;
        }

        else if (obt >= 60 && obt < 70) {
            document.getElementById("gpMath").value = 3.5;
        }
        else if (obt >= 50 && obt < 60) {
            document.getElementById("gpMath").value = 3;
        }
        else if (obt >= 40 && obt < 50) {
            document.getElementById("gpMath").value = 2;
        }
        else if (obt >= 33 && obt < 40) {
            document.getElementById("gpMath").value = 1;
        }
        else {
            document.getElementById("gpMath").value = 0;
        }
        
        var bnObtain = parseFloat(document.getElementById("banglaObtain").value);
        var enObtain = parseFloat(document.getElementById("englishObtain").value);
        var mathObtain = parseFloat(document.getElementById("mathObtain").value);
        var totalObtain = bnObtain + enObtain + mathObtain;
        document.getElementById("totalGrandObtain").value = totalObtain;
        
        var bnGrade = parseFloat(document.getElementById("gpbangla").value);
        var enGrade = parseFloat(document.getElementById("gpEnglish").value);
        var mathGrade = parseFloat(document.getElementById("gpMath").value);
        
        var grade = parseFloat(bnGrade + enGrade + mathGrade) / 3;
        document.getElementById("totalGpa").value = grade;

        var totalGp = parseFloat(document.getElementById("totalGpa").value);
        if (totalGp >= 5) {
            document.getElementById("totalGrade").value = "A+";
        }
        else if (totalGp >= 4 && totalGp < 5) {
            document.getElementById("totalGrade").value = "A";
        }

        else if (totalGp >= 3.5 && totalGp < 4) {
            document.getElementById("totalGrade").value = "A-";
        }
        else if (totalGp >= 3 && totalGp < 3.5) {
            document.getElementById("totalGrade").value = "B";
        }
        else if (totalGp >= 2 && totalGp < 3) {
            document.getElementById("totalGrade").value = "C";
        }
        else if (totalGp >= 1 && totalGp < 2) {
            document.getElementById("totalGrade").value = "D";
        }
        else {
            document.getElementById("totalGrade").value = "F";
        }

    }
</script>




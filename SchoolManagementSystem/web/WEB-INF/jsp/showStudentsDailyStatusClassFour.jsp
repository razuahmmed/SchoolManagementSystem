<%-- 
    Document   : showStudentsDailyStatusClassFour
    Created on : Oct 16, 2015, 12:48:38 AM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <table>
                <th>
                <tr>
                    <td>Roll Number</td>
                    <td>Student Name</td>
                    <td>Date</td>
                    <td>Status</td>
                    <td>Count Absent</td>
                    <td>Count Present</td>
                    <td>Teacher's Comment</td>
                </tr>
                </th>
                <c:forEach var="list" items="${List}">
                    <tr>
                        <td>${list.classFourAdmissionTable.rollNumberFour}</td>
                        <td>${list.studentName}</td>
                        <td>${list.date}</td>
                        <td>${list.status}</td>
                        <td>${list.countAbsent}</td>
                        <td>${list.countPresent}</td>
                        <td>${list.classTeacher}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>



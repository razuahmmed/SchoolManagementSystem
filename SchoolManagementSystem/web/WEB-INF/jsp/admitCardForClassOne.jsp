<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6" id="printableArea">
            <fieldset>
                <legend>Admit Card For Class One</legend>
                <table class="table">
                    <tr>
                        <td>Class </td>
                        <td> One</td>
                        <td>Date</td>
                        <td>${Date}</td>
                    </tr>
                    <tr>
                        <td>Serial Number</td>
                        <td>${Student.serialNumberOneadtest}</td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td>${Student.studentName}</td>
                        <td></td>
                        <td><img  src="<c:url value="${Student.image}"></c:url>" style=" height: 50px; width: 90px;"/></td>
                        </tr>
                        <tr>
                            <td>Father Name</td>
                            <td>${Student.fatherName}</td>
                    </tr>
                    <tr>
                        <td>Mother Name</td>
                        <td>${Student.motherName}</td>
                    </tr>
                    <tr>
                        <td>Exam Date</td>
                        <td>15-Nov-2015</td>
                    </tr>
                    <tr>
                        <td>Exam Time</td>
                        <td>09.30 AM</td>
                    </tr>
                    <tr>
                        <td>Exam Center</td>
                        <td>Government Laboratory School, Dhanmondi, Dhaka</td>
                    </tr>
                    <tr>
                        <td>Controller</td>
                        <td>Atiqur Rahaman</td>
                    </tr>
                </table>
            </fieldset>


        </div>
        <input type="button" onclick="printDiv('printableArea')" value="Print!" />
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById("printableArea").innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
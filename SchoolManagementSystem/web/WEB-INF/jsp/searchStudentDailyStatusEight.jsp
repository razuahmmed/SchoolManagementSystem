<%-- 
    Document   : searchStudentDailyStatusEight
    Created on : Oct 16, 2015, 12:40:32 AM
    Author     : Maksud Rahaman
--%>


<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <form action="searchStudentStatusClassEight.htm">
                <fieldset>
                    <legend>
                        Class Eight Attendance
                    </legend>
                    <table>
                        <th>
                        <tr>
                            <td>Roll Number</td>

                        </tr> 
                        </th>
                        <tr>
                            <td><input type="int" name="rollNumberEight"></td>

                        </tr> 
                        <tr>

                            <td><input type="submit"  value="Submit"></td>

                        </tr> 

                    </table>
                </fieldset>


            </form>
        </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>


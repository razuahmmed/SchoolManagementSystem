

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="admissionForClassSix">
            <fieldset>
                <legend>Fill up the Form with Correct Information</legend>
                <table>
                    <tr>
                        <td>Serial Number</td>
                        <td><input type="text" name="serialNumber"/></td>
                    </tr>
                    <tr>
                        <td>Student's Name</td>
                        <td><input type="text" name="studentName"/></td>
                    </tr>
                    <tr>
                        <td>Father's Name</td>
                        <td><input type="text" name="fatherName"/></td>
                    </tr>
                    <tr>
                        <td>Mother's Name</td>
                        <td><input type="text" name="motherName"/></td>
                    </tr>
                    <tr>
                        <td>Date of Birth</td>
                        <td><input type="text" name="dateOfBirth"/></td>
                    </tr>
                    <tr>
                        <td>Present Address</td>
                        <td><input type="text" name="presentAddress"/></td>
                    </tr>
                    <tr>
                        <td>Permanent Address</td>
                        <td><input type="text" name="permanentAddress"/></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td>
                            <input type="radio" name="gender" value="Male"/>Male &nbsp;
                            <input type="radio" name="gender" value="Female"/>Female &nbsp;
                            <input type="radio" name="gender" value="Third"/>Third &nbsp;
                        </td>

                    </tr>
                    <tr>
                        <td>Religion</td>
                        <td>
                            <input type="radio" name="religion" value="Islam"/>Islam &nbsp;
                            <input type="radio" name="religion" value="Hinduism"/>Hinduism &nbsp;
                            <input type="radio" name="religion" value="Christian"/>Christian &nbsp;
                            <input type="radio" name="religion" value="Buddism"/>Buddhism &nbsp;
                            <input type="radio" name="religion" value="Others"/>Others &nbsp;
                        </td>

                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><input type="text" name="email"/></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><input type="text" name="phone"/></td>
                    </tr>
                    <tr>
                        <td>Nationality</td>
                        <td><input type="text" name="nationality"/></td>
                    </tr>
                    <tr>
                        <td>Division</td>
                        <td>
                            <select name="division">
                                <option>Select Division</option>
                                <option value="Dhaka">Dhaka</option>
                                <option value="Chittagong">Chittagong</option>
                                <option value="Rajshahi">Rajshahi</option>
                                <option value="Sylhet">Sylhet</option>
                                <option value="Rongpur">Rangpur</option>
                                <option value="Barishal">Barishal</option>
                                <option value="Khuulna">Khulna</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>District</td>
                        <td><input type="text" name="district"/></td>
                    </tr>
                    <tr>
                        <td>Image</td>
                        <td><input type="text" name="image"/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </body>
</html>

<%-- 
    Document   : showAllTeachersInformtion
    Created on : Oct 19, 2015, 8:38:02 AM
    Author     : Maksud Rahaman
--%>


<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <!--        <div class="col-sm-3 subDropdown">
                   
                </div>-->
        <div class="col-sm-6">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <fieldset>
                <legend>Class Five Student Information</legend>
                <table class="table">
                    
                       <th>
                    <tr>
                        <td>Teacher's Identity</td>
                        
                        <td>Teacher's Name</td>
                        

                        <td>Group</td>
                        
                        <td>Designation</td>
                       
                        <td>Join Date</td>
                       
                        <td>Email</td>
                        
                        <td>Phone</td>
                        

                        <td>Gender</td>
                      
                        <td>Educational Qualification</td>
                        
                        <td>Image</td>
                        

                    </tr>
                    </th> 
                    
                    
                    <c:forEach var="list" items="${List}">
                        <tr>
                            <td>${list.teacherId}</td>
                            
                            <td>${list.teacherName}</td>
                            
                            <td>${list.group}</td>
                           
                            <td>${list.designation}</td>
                           
                            <td>${list.joinDate}</td>
                            
                            <td>${list.email}</td>
                            
                            <td>${list.phone}</td>
                            
                            <td>${list.gender}</td>
                            
                            <td>${list.educationalQualification}</td>
                            

                            <td><img width="50px" height="50px" src="<c:url value="${list.image}"></c:url>"></td>
                                

                            </tr>
                    </c:forEach>
                </table>
            </fieldset>
        </div>

    </div>
</div>

<%@ include file="footer.jsp" %>

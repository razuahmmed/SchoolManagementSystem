<%@ include file="header.jsp" %>
<div class="container">
    
    <div class="row">
        <div class="col-sm-3 subDropdown padzero">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6 padzero">
            <div id="wowslider-container1" class="ws_gestures" >
                <div class="ws_images" style="overflow: visible;">
                    <div class="ws_list" style="position: absolute; top: 0px; height:auto; transform: translate3d(0px, 0px, 0px); ">
                        <ul style="width: 100%;">
                            <li ><img src="<c:url value='/images/img/img1.jpg'/>" alt="Stylist Watch" title="Stylist APU Watch" id="wows1_0" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img2.jpg'/>" alt="Samsung GALAXY" title="Samsung APU GALAXY" id="wows1_1" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img3.jpg'/>" alt="iPhone 5" title="iPhone 5" id="wows1_2" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img4.jpg'/>" alt="carousel jquery" title="Microsoft Windows Phone" id="wows1_3" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img5.jpg'/>" alt="Black Berry" title="Black Berry" id="wows1_4" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img6.jpg'/>" alt="Black Berry" title="Black Berry" id="wows1_4" style="visibility: visible;"></li>
                             <li ><img src="<c:url value='/images/img/img7.jpg'/>" alt="Stylist Watch" title="Stylist APU Watch" id="wows1_0" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img8.jpg'/>" alt="Samsung GALAXY" title="Samsung APU GALAXY" id="wows1_1" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img12.jpg'/>" alt="iPhone 5" title="iPhone 5" id="wows1_2" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img9.jpg'/>" alt="carousel jquery" title="Microsoft Windows Phone" id="wows1_3" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img10.jpg'/>" alt="Black Berry" title="Black Berry" id="wows1_4" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img11.jpg'/>" alt="Black Berry" title="Black Berry" id="wows1_4" style="visibility: visible;"></li>
                             <li ><img src="<c:url value='/images/img/img13.jpg'/>" alt="Stylist Watch" title="Stylist APU Watch" id="wows1_0" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img14.jpg'/>" alt="Samsung GALAXY" title="Samsung APU GALAXY" id="wows1_1" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img15.jpg'/>" alt="iPhone 5" title="iPhone 5" id="wows1_2" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img16.jpg'/>" alt="carousel jquery" title="Microsoft Windows Phone" id="wows1_3" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img17.jpg'/>" alt="Black Berry" title="Black Berry" id="wows1_4" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img18.jpg'/>" alt="Black Berry" title="Black Berry" id="wows1_4" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img19.jpg'/>" alt="Black Berry" title="Black Berry" id="wows1_4" style="visibility: visible;"></li>
                            <li ><img src="<c:url value='/images/img/img20.jpg'/>" alt="Black Berry" title="Black Berry" id="wows1_4" style="visibility: visible;"></li>
                        
                        </ul>
                    </div>
                    <div class="ws_controls">
                        <div class="ws_bullets">
                            <div>
                                <a href="#" title="Stylist Watch" class="ws_selbull"><span>1</span></a>
                                <a href="#" title="Samsung APU GALAXY" class=""><span>2</span></a>
                                <a href="#" title="iPhone 5" class=""><span>3</span></a>
                                <a href="#" title="Microsoft Windows Phone" class=""><span>4</span></a>
                                <a href="#" title="Black APU Berry" class=""><span>5</span></a>
                                <a href="#" title="Black APU Berry" class=""><span>6</span></a>
                                 <a href="#" title="Stylist Watch" class="ws_selbull"><span>1</span></a>
                                <a href="#" title="Samsung APU GALAXY" class=""><span>2</span></a>
                                <a href="#" title="iPhone 5" class=""><span>3</span></a>
                                <a href="#" title="Microsoft Windows Phone" class=""><span>4</span></a>
                                <a href="#" title="Black APU Berry" class=""><span>5</span></a>
                                <a href="#" title="Black APU Berry" class=""><span>6</span></a>
                            </div>
                        </div>
                        <a href="#" class="ws_next"><span><i></i><b></b></span></a>
                        <a href="#" class="ws_prev"><span><i></i><b></b></span></a>
                        <a href="#" class="ws_playpause ws_pause"><span><i></i><b></b></span></a>
                    </div>

                </div>   
            </div>
                        
                        <h1 style="color: red; font-size: 20px">${ErrorMessage}</h1>
                        <h1 style="color: green; font-size: 20px">${SuccessMessage}</h1>
                       
            <div class="para">
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type
                    specimen book. It has survived not only five centuries, but also the leap into
                    electronic typesetting, remaining essentially unchanged. It was popularised in
                    the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                    and more recently with desktop publishing software like Aldus PageMaker
                    including versions of Lorem Ipsum.</p>
                <p>
                    Contrary to popular belief, Lorem Ipsum is not simply random text.
                    It has roots in a piece of classical Latin literature from 45 BC, 
                    making it over 2000 years old. Richard McClintock, a Latin professor 
                    at Hampden-Sydney College in Virginia, looked up one of the more
                    obscure Latin words, consectetur, from a Lorem Ipsum passage, 
                    and going through the cites of the word in classical literature, 
                    discovered the undoubtable source. Lorem Ipsum comes from sections 
                    1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil)
                    by Cicero, written in 45 BC. This book is a treatise on 
                    the theory of ethics, very popular during the Renaissance. 
                    The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet.
                    .", comes from a line in section 1.10.32.</p>
            </div>
        </div>
        <div class="col-sm-3 subDropdown padzero">
            <nav>
                <div class="menu-item ">
                    <h4><a href="#">Lorem ipsum dolor&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                    <ul>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">incididunt ut labore  </a></li>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">  </a></li>


                    </ul>
                </div>

                <div class="menu-item alpha">
                    <h4><a href="#">nostrud exercita &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                    <ul>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">incididunt ut labore  </a></li>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">  </a></li>
                    </ul>

                </div>

                <div class="menu-item alpha">
                    <h4><a href="#">ullamco laboris  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                    <ul>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">incididunt ut labore  </a></li>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">  </a></li>

                    </ul>

                </div>

                <div class="menu-item alpha ">
                    <h4><a href="#">nisi ut aliquip ex  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                    <ul>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">incididunt ut labore  </a></li>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">  </a></li>

                    </ul>

                </div>
                <div class="menu-item alpha ">
                    <h4><a href="#">ea commodo  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                    <ul>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">incididunt ut labore  </a></li>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">  </a></li>

                    </ul>

                </div>
                <div class="menu-item ">
                    <h4><a href="#">Lorem ipsum dolor&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                    <ul>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">incididunt ut labore  </a></li>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">  </a></li>


                    </ul>
                </div>

                <div class="menu-item alpha">
                    <h4><a href="#">nostrud exercita &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                    <ul>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">incididunt ut labore  </a></li>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">  </a></li>
                    </ul>

                </div>

                <div class="menu-item alpha">
                    <h4><a href="#">ullamco laboris  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                    <ul>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">incididunt ut labore  </a></li>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">  </a></li>

                    </ul>

                </div>

                <div class="menu-item alpha ">
                    <h4><a href="#">nisi ut aliquip ex  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                    <ul>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">incididunt ut labore  </a></li>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">  </a></li>

                    </ul>

                </div>
                <div class="menu-item alpha ">
                    <h4><a href="#">ea commodo  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                    <ul>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">incididunt ut labore  </a></li>
                        <li><a href="#">amet consectetur  </a></li>
                        <li><a href="#">adipiscing elit sed do</a></li>
                        <li><a href="#">eiusmod tempor   </a></li>
                        <li><a href="#">  </a></li>

                    </ul>

                </div>
            </nav> 
        </div>
    </div>
</div>
<%@ include file="footer.jsp" %>